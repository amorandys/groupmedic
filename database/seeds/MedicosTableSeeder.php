<?php

use Illuminate\Database\Seeder;

class MedicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicos')->insert([
                'nombres'           => 'Oscar Fernando',
                'apellidos'         => 'Mora Gonzalez',
                'rut'               => 17472758,
                'rut_v'             => 9,
                'fecha_nacimiento'  => date('Y-m-d', strtotime('04/08/1986')),
                'sexo'              => 'masculino',
                'especialidad_id'   => null,
                'user_id'           => 2,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),

            ]);

        DB::table('medico_contactos')->insert([
                'telefono'      => '+56 2 9875 4157',
                'celular'       => '+56 9 4567 3215',
                'email'         => 'omora@gmail.com',
                'direccion'     => 'Valparaiso 1547',
                'region_id'     => 81,
                'comuna_id'     => 443687,
                'medico_id'     => 1,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);
    }
}
