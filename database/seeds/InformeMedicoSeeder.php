<?php

use Illuminate\Database\Seeder;

class InformeMedicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inf_medico')->insert([
        	'medico_id' 	=> 1,
        	'paciente_id' 	=> 1,
        	'sucursal_id' 	=> 1,
        	'diagnostico' 	=> '1.- Enfermedad Renal Cronica en Hemodialisis trisemanal <br />.- Diabetes Mellitus 2 IR con DOB <br /> 3.- Usuaria marcapaso por BAVC <br /> 4.- TACO <br />5.- HTA larga data<br /> 6.- Anemia multifactorial <br />7.- úlcera pie diabetico ínfectada?',
        	'tratamiento' 	=> '"1.- Aspirina 100 mg por dia <br />2.- Atorvastatina 40 mg por noche <br />3.- Etalpha 0.5 mcg post hemodialisis <br />4.- Nifedipino 10 mg cada 8 horas <br />5.- Enalapril 10 mg cada 12 horas <br />6.- Fercovit 1 por dia <br />7.- Omeprazol 20 mg por dia <br />8.- EPO 4000 ui por semana',
        	'observaciones' => '"Paciente requiere evaluacion precoz por Vascular o policlinico. Pie diabetico con ulcera con secrecion seropurulenta? de mal olor, ínfeccion por Pseudomona?',
        	'fecha_informe' => date('Y-m-d'),
        	'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ]);
    }
}
