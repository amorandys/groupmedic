<?php

use Illuminate\Database\Seeder;

class SucursalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sucursales')->insert([
            'nombre' 	=> 'Centro Maipu',
            'direccion' => 'Barros Arana 123',
            'telefono' 	=> '+56 2 2745 8686',
            'email' 	=> 'maipu@cenmed.cl',
        ]);

        DB::table('sucursales')->insert([
            'nombre'    => 'Santiago Centro',
            'direccion' => 'Catedral 1464',
            'telefono'  => '+56 2 2745 9886',
            'email'     => 'scentro@cenmed.cl',
        ]);

        DB::table('user_sucursales')->insert([
            'user_id'       => 1,
            'sucursal_id'   => 1,
        ]);

        DB::table('user_sucursales')->insert([
            'user_id'       => 1,
            'sucursal_id'   => 2,
        ]);

        DB::table('user_sucursales')->insert([
            'user_id'       => 2,
            'sucursal_id'   => 1,
        ]);

        DB::table('user_sucursales')->insert([
            'user_id'       => 3,
            'sucursal_id'   => 1,
        ]);
    }
}
