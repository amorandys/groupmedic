<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'usuario' => 'Rosmar Blasquez',
            'email' => 'reblasquez@gmail.com',
            'login' => 'rblasquez',
            'password' => bcrypt('y6f47y'),
            'tipo' => 1,
        ]);

        DB::table('users')->insert([
            'usuario' => 'Oscar Mora',
            'email' => 'omora@gmail.com',
            'login' => 'omora',
            'password' => bcrypt('y6f47y'),
            'tipo' => 2,
        ]);

        DB::table('users')->insert([
            'usuario' => 'Administrador',
            'email' => 'scinformatica@gmail.com',
            'login' => 'admin',
            'password' => bcrypt('sc123456'),
            'tipo' => 1,
        ]);

        DB::table('role_user')->insert([
                'user_id' => 1,
                'role_id' => 1,
            ]);

        DB::table('role_user')->insert([
                'user_id' => 2,
                'role_id' => 2,
            ]);

        DB::table('role_user')->insert([
                'user_id' => 3,
                'role_id' => 1,
            ]);
    }
}
