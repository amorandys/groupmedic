<?php

use Illuminate\Database\Seeder;

class PacientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pacientes')->insert([
	    		'rut' 					=> '19530692',
	    		'rut_v' 				=> '3',
	    		'nombres' 				=> 'Oscar Fernadez',
	    		'apellidos' 			=> 'Mora Gonzalez',
	    		'sexo' 					=> 'masculino',
                'fecha_nacimiento'  	=> date('Y-m-d', strtotime('04/08/1986')),
	    		'pais_nacionalidad_id' 	=> '95',
	    		'pais_nacimiento_id' 	=> '95',
	    		'created_at' 			=> date('Y-m-d H:i:s'),
	    		'updated_at' 			=> date('Y-m-d H:i:s'),
    		]);

        DB::table('paciente_contactos')->insert([
	    		'paciente_id' 			=> 1,
	    		'email' 				=> 'omora@email.com',
	    		'telefono' 				=> '+56 2 2165 4867',
	    		'celular' 				=> '+56 9 3514 3842',
	    		'direccion' 			=> 'Santiago Centro',
	    		'created_at' 			=> date('Y-m-d H:i:s'),
	    		'updated_at' 			=> date('Y-m-d H:i:s'),
    		]);
    }
}
