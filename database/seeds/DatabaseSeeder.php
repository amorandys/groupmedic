<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PermisosTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        
        $this->call(SucursalTableSeeder::class);
        $this->call(PacientesTableSeeder::class);
        $this->call(MedicosTableSeeder::class);
        
        # Informes
        $this->call(InformeMedicoSeeder::class);
        
        /*
        $this->call(PaisesTableSeeder::class);
        $this->call(RegionesTableSeeder::class);
        $this->call(CiudadesTableSeeder::class);
        */
    }
}
