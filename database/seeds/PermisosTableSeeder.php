<?php

use Illuminate\Database\Seeder;

class PermisosTableSeeder extends Seeder
{
    public function run()
    {
    	$data = [
    		[
    			'name' 			=> 'usuarios_create',
    			'display_name' 	=> 'Crear usuarios',
    			'description' 	=> 'Agregar usuarios al sistema.',
    		],
    		[
    			'name' 			=> 'usuarios_edit',
    			'display_name' 	=> 'Editar usuarios',
    			'description' 	=> 'Editar la información de los usuarios.',
    		],
    		[
    			'name' 			=> 'usuarios_delete',
    			'display_name' 	=> 'eliminar_usuarios',
    			'description' 	=> 'Eliminar toda la información de los usuarios.',
    		],
    		[
    			'name' 			=> 'usuarios_show',
    			'display_name' 	=> 'Visualizar usuarios',
    			'description' 	=> 'Visualizar la información de los usuarios.',
    		],
    		[
    			'name' 			=> 'paciente_create',
    			'display_name' 	=> 'Crear pacientes',
    			'description' 	=> 'Registrar pacientes en el sistema.',
    		],
    		[
    			'name' 			=> 'paciente_edit',
    			'display_name' 	=> 'Editar pacientes',
    			'description' 	=> 'Editar la información de los pacientes.',
    		],
    		[
    			'name' 			=> 'paciente_delete',
    			'display_name' 	=> 'Eliminar pacientes',
    			'description' 	=> 'Eliminar los registros de los pacientes.',
    		],
    		[
    			'name' 			=> 'paciente_show',
    			'display_name' 	=> 'Visualiar pacientes',
    			'description' 	=> 'Visualizar la información de los pacientes',
    		],

    		[
    			'name' 			=> 'medico_create',
    			'display_name' 	=> 'Crear médicos',
    			'description' 	=> 'Registrar médicos en el sistema.',
    		],
    		[
    			'name' 			=> 'medico_edit',
    			'display_name' 	=> 'Editar médicos',
    			'description' 	=> 'Editar la información de los médicos.',
    		],
    		[
    			'name' 			=> 'medico_delete',
    			'display_name' 	=> 'Eliminar médicos',
    			'description' 	=> 'Eliminar los registros de los médicos.',
    		],
    		[
    			'name' 			=> 'medico_show',
    			'display_name' 	=> 'Visualiar médicos',
    			'description' 	=> 'Visualizar la información de los médicos',
    		],

            [
                'name'          => 'agenda_create',
                'display_name'  => 'Crear agenda',
                'description'   => 'Registrar información en la agenda médica.',
            ],
            [
                'name'          => 'agenda_edit',
                'display_name'  => 'Editar agenda',
                'description'   => 'Editar la información de la agenda médica.',
            ],
            [
                'name'          => 'agenda_delete',
                'display_name'  => 'Eliminar agenda',
                'description'   => 'Eliminar los registros de la agenda médica.',
            ],
            [
                'name'          => 'agenda_show',
                'display_name'  => 'Visualiar agenda',
                'description'   => 'Visualizar la información de la agenda médica',
            ],
    		
    	];

    	foreach ($data as $key => $value) {
        	DB::table('permissions')->insert([
		    		'name' 			=> $value['name'],
		    		'display_name' 	=> $value['display_name'],
		    		'description' 	=> $value['description'],
		    		'created_at' 	=> date('Y-m-d H:i:s'),
		    		'updated_at' 	=> date('Y-m-d H:i:s'),
	    		]);
    	}

        $role_permisos = [
                    ['permission_id' => 1,  'role_id' => 1],
                    ['permission_id' => 2,  'role_id' => 1],
                    ['permission_id' => 3,  'role_id' => 1],
                    ['permission_id' => 4,  'role_id' => 1],
                    ['permission_id' => 5,  'role_id' => 1],
                    ['permission_id' => 6,  'role_id' => 1],
                    ['permission_id' => 7,  'role_id' => 1],
                    ['permission_id' => 8,  'role_id' => 1],
                    ['permission_id' => 9,  'role_id' => 1],
                    ['permission_id' => 10, 'role_id' => 1],
                    ['permission_id' => 11, 'role_id' => 1],
                    ['permission_id' => 12, 'role_id' => 1],
                    ['permission_id' => 13, 'role_id' => 1],
                    ['permission_id' => 14, 'role_id' => 1],
                    ['permission_id' => 15, 'role_id' => 1],
                    ['permission_id' => 16, 'role_id' => 1],

                    ['permission_id' => 6,  'role_id' => 2],
                    ['permission_id' => 8,  'role_id' => 2],
                    ['permission_id' => 10, 'role_id' => 2],
                    ['permission_id' => 12, 'role_id' => 2],
                    ['permission_id' => 13, 'role_id' => 2],
                    ['permission_id' => 14, 'role_id' => 2],
                    ['permission_id' => 15, 'role_id' => 2],
                    ['permission_id' => 16, 'role_id' => 2],
                ];

            foreach ($role_permisos as $key => $value) {
            DB::table('permission_role')->insert([
                    'permission_id' => $value['permission_id'],
                    'role_id'       => $value['role_id'],
                ]);
        }
    }
}
