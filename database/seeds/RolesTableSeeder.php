<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('roles')->insert([
	    		'name' 			=> 'admin',
	    		'display_name' 	=> 'Administrador',
	    		'description' 	=> 'Administrador general del sistema',
	    		'created_at' 	=> date('Y-m-d H:i:s'),
	    		'updated_at' 	=> date('Y-m-d H:i:s'),
    		]);

    	DB::table('roles')->insert([
	    		'name' 			=> 'med',
	    		'display_name' 	=> 'Médico',
	    		'description' 	=> 'Medico con acceso al sistema',
	    		'created_at' 	=> date('Y-m-d H:i:s'),
	    		'updated_at' 	=> date('Y-m-d H:i:s'),
    		]);

    	DB::table('roles')->insert([
	    		'name' 			=> 'enf',
	    		'display_name' 	=> 'Enfermera',
	    		'description' 	=> 'Enfermera con acceso al sistema',
	    		'created_at' 	=> date('Y-m-d H:i:s'),
	    		'updated_at' 	=> date('Y-m-d H:i:s'),
    		]);

    	DB::table('roles')->insert([
	    		'name' 			=> 'sec',
	    		'display_name' 	=> 'Secretaria',
	    		'description' 	=> 'Secretaria con acceso al sistema',
	    		'created_at' 	=> date('Y-m-d H:i:s'),
	    		'updated_at' 	=> date('Y-m-d H:i:s'),
    		]);
    }
}
