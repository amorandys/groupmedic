<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicoContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medico_contactos', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('telefono');
            $table->string('celular');
            $table->string('email', 100);
            $table->string('direccion')->nullable();
            $table->integer('region_id')->unsigned();
            $table->integer('comuna_id')->unsigned();
            $table->integer('medico_id')->unsigned();
            $table->timestamps();
        
        });

        Schema::table('medico_contactos', function($table) {
            $table->foreign('region_id')->references('id')->on('regiones')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('comuna_id')->references('id')->on('comunas')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medico_contactos');
    }
}
