<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut');
            $table->tinyInteger('rut_v');
            $table->string('nombres');
            $table->string('apellidos');
            $table->enum('sexo', ['masculino', 'femenino']);
            $table->date('fecha_nacimiento');
            $table->integer('pais_nacionalidad_id')->unsigned();
            $table->integer('pais_nacimiento_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('pacientes', function($table) {
            $table->foreign('pais_nacionalidad_id')->references('id')->on('paises')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('pais_nacimiento_id')->references('id')->on('paises')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
