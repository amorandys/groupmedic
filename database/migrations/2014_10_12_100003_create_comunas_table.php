<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pais_id')->unsigned();
            $table->integer('region_id')->unsigned();
            $table->string('descripcion');
            $table->timestamps();
        });

        Schema::table('comunas', function($table) {
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('region_id')->references('id')->on('regiones')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunas');
    }
}
