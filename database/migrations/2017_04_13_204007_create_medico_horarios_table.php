<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicoHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medico_horarios', function (Blueprint $table) {
            
            $table->increments('id');
            $table->enum('dia', ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo']);
            $table->enum('turno', ['mañana', 'tarde', 'noche']);
            $table->time('hora_ini');
            $table->time('hora_fin');
            $table->integer('medico_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('medico_horarios', function($table) {
            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medico_horarios');
    }
}
