<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfNefrologiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inf_nefrologia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('medico_id')->unsigned()->nullable();
            $table->integer('paciente_id')->unsigned()->nullable();
            $table->integer('sucursal_id')->unsigned()->nullable();
            $table->string('diagnostico');

            $table->string('d_peso', 15)->nullable();
            $table->string('d_qb', 15)->nullable();
            $table->string('d_qtipo', 15)->nullable();
            $table->string('d_flujo', 15)->nullable();
            $table->string('d_filtro', 15)->nullable();
            $table->string('d_dca', 15)->nullable();
            $table->string('d_dfiltro', 15)->nullable();
            $table->string('d_heparina', 15)->nullable();
            $table->string('d_tipo_acceso_vascular', 15)->nullable();
            $table->string('d_k', 15)->nullable();
            $table->string('d_ca', 15)->nullable();
            $table->string('d_bicarbonato', 15)->nullable();
            $table->string('l_hto', 15)->nullable();
            $table->string('l_saturacion', 15)->nullable();
            $table->string('l_ferritina', 15)->nullable();
            $table->string('l_ktv', 15)->nullable();
            $table->string('l_bun', 15)->nullable();
            $table->string('l_creatinina', 15)->nullable();
            $table->string('l_albumina', 15)->nullable();
            $table->string('l_ast', 15)->nullable();
            $table->string('l_k', 15)->nullable();
            $table->string('l_ca', 15)->nullable();
            $table->string('l_p', 15)->nullable();
            $table->string('l_falcalina', 15)->nullable();
            $table->string('l_pth', 15)->nullable();
            $table->string('l_hiv', 15)->nullable();
            $table->string('l_vhb', 15)->nullable();
            $table->string('l_vhc', 15)->nullable();
            
            $table->string('tratamiento');
            $table->string('observaciones')->nullable();
            $table->date('fecha_informe');
            $table->timestamps();
        });

        Schema::table('inf_nefrologia', function($table) {
            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inf_nefrologia');
    }
}
