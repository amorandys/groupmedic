<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfMedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inf_medico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('medico_id')->unsigned()->nullable();
            $table->integer('paciente_id')->unsigned()->nullable();
            $table->integer('sucursal_id')->unsigned()->nullable();

            $table->longText('diagnostico');
            $table->longText('tratamiento');
            $table->longText('observaciones')->nullable();

            $table->date('fecha_informe');
            $table->timestamps();
        });

        Schema::table('inf_medico', function($table) {
            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inf_medico');
    }
}

