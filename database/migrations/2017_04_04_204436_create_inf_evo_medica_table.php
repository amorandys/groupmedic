<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfEvoMedicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inf_evo_medica', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medico_id')->unsigned()->nullable();
            $table->integer('paciente_id')->unsigned()->nullable();
            $table->integer('sucursal_id')->unsigned()->nullable();
            
            $table->string('estado');
            $table->string('tolerancia_hd');
            $table->string('compli_intra')->nullable();
            $table->string('peso_seco')->nullable();
            $table->string('causa_compli_intra')->nullable();
            $table->string('compli_cardio')->nullable();
            $table->string('arritmia')->nullable();
            $table->string('arritmia_causa')->nullable();
            $table->string('edo_hermatologico')->nullable();
            $table->string('usa_epo_dosis')->nullable();
            $table->string('hierro')->nullable();
            $table->string('transfusion_cantidad')->nullable();
            $table->string('edo_osteometabolico')->nullable();
            $table->string('deriv_vit_d')->nullable();
            $table->string('dep_urea')->nullable();
            $table->string('edo_nutricional')->nullable();
            $table->string('acc_vascular')->nullable();
            $table->string('plan_terapeutico')->nullable();
            $table->string('urgencia_hospitalizacion')->nullable();
            $table->string('resuelto')->nullable();
            $table->string('motivo_consulta')->nullable();
            $table->string('evaluacion')->nullable();
            $table->string('solicitud_rx')->nullable();
            $table->string('progragrama_tx')->nullable();

            $table->date('fecha_informe');
            $table->timestamps();
        });

        Schema::table('inf_evo_medica', function($table) {
            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inf_evo_medica');
    }
}


#?? est_codigo
#?? tol_codigo

