<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sucursales', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('sucursal_id')->unsigned();
        });

        Schema::table('user_sucursales', function($table) {
            $table->primary(['user_id', 'sucursal_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sucursales');
    }
}
