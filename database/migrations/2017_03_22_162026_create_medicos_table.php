<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicos', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->bigInteger('rut');
            $table->tinyInteger('rut_v');
            $table->date('fecha_nacimiento');
            $table->enum('sexo', ['masculino', 'femenino']);
            $table->integer('especialidad_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->timestamps();

        });

        Schema::table('medicos', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL')->onUpdate('CASCADE');
            $table->foreign('especialidad_id')->references('id')->on('especialidades')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicos');
    }
}
