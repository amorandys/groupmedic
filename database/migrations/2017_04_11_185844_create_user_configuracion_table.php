<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConfiguracionTable extends Migration
{
    public function up()
    {
        Schema::create('user_configuracion', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->integer('user_id')->unsigned();
            $table->integer('sucursal_id')->unsigned()->nullable();
            
            $table->timestamps();
        });

        Schema::table('user_configuracion', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUptate('CASCADE');
            $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('SET NULL')->onUptate('CASCADE');
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_configuracion');
    }
}
