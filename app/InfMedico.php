<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfMedico extends Model
{
    protected $table = 'inf_medico';

    protected $guarded = [];

    public function medico()
    {
    	return $this->belongsTo('App\Medico');
    }

    public function paciente()
    {
    	return $this->belongsTo('App\Paciente');
    }

    public function sucursal()
    {
        return $this->belongsTo('App\Sucursal');
    }
}
