<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'pacientes';

    protected $guarded = [];


    public function contacto()
    {
        return $this->hasOne('App\PacienteContacto');
    }

    public function diagnostico()
    {
        return $this->hasOne('App\PacienteDiagnostico');
    }

    //HISTORIAL MEDICO DEL PACIENTE

    public function infMedico()
    {
        return $this->hasMany('App\InfMedico');
    }

    public function infNefrologico()
    {
        return $this->hasMany('App\InfNefrologia');
    }

    public function infEvoMedica()
    {
        return $this->hasMany('App\InfEvoMedica');
    }
}
