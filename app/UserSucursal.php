<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSucursal extends Model
{
    protected $table = 'user_sucursales';

    protected $guarded = [];
    
    public $timestamps = false;
    public $incrementing = false;

    public function sucursal()
    {
    	return $this->belongsTo('App\Sucursal');
    }
}
