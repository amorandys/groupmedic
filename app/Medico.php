<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    protected $table = 'medicos';

    protected $guarded = [];

    public function contacto()
    {
    	return $this->hasOne('App\MedicoContacto');
    }

    public function horario()
    {
    	return $this->hasMany('App\MedicoHorario');
    }

}
