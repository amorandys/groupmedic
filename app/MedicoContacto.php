<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicoContacto extends Model
{
    protected $table = 'medico_contactos';

    protected $guarded = [];
}
