<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteContacto extends Model
{
    protected $table = 'paciente_contactos';

    protected $guarded = [];
}
