<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;
use App\Permission;

use DB;

class RolesController extends Controller
{
    public function index()
    {
    	return view('auth.roles.index', [
    			'roles' => DB::select('CALL listado_roles()'),
    		]);
    }

    public function edit($id)
    {
    	$role = Role::find($id);
    	$role->perms;
    	return view('auth.roles.edit', [
    			'role' => Role::find($id),
    			'permisos' => DB::select('CALL listado_permisos()')
    			]);
    }

    public function update(Request $request, $id)
    {
    	$role = Role::find($id);
    	if ($request->perm) {
    		$role->perms()->sync($request->perm);
    	}else{
    		$role->perms()->sync([]);
    	}

		alert()->success('Los permisos fueron actualizados con exito.', '¡Bien hecho!');
    	return back();
    }
}
