<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sucursal;
use App\InfEvoMedica;
use App\Paciente;

use Auth;

class EvolucionController extends Controller
{
    public function index()
    {
        $sucursales = Sucursal::pluck('nombre', 'id');
    	return view('ficham.evolucion.index', ['sucursales' => $sucursales]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $paciente = Paciente::where('rut', str_replace('.', '', $request->rut_paciente))
            ->where('rut_v', digitoVerificador($request->rut_paciente))
            ->first();
        
        $medico = Auth::user()->medico;

        if (!$medico) {
            alert()->error('El usuario no esta asociado a un médico, no puede realizar esta operación', 'Error')->persistent('Aceptar');
            return back()->withInput();
        }

        $informe = InfEvoMedica::firstOrCreate([
                'medico_id'                 => $medico->id,
                'paciente_id'               => $paciente->id,
                'sucursal_id'               => Auth::user()->configuracion->sucursal->id,
                
                'estado'                    => $request->estado,
                'tolerancia_hd'             => $request->tolerancia_hd,
                'compli_intra'              => $request->compli_intra,
                'peso_seco'                 => $request->peso_seco,
                'causa_compli_intra'        => $request->causa_compli_intra,
                'compli_cardio'             => $request->compli_cardio,
                'arritmia'                  => $request->arritmia,
                'arritmia_causa'            => $request->arritmia_causa,
                'edo_hermatologico'         => $request->edo_hermatologico,
                'usa_epo_dosis'             => $request->usa_epo_dosis,
                'hierro'                    => $request->hierro,
                'transfusion_cantidad'      => $request->transfusion_cantidad,
                'edo_osteometabolico'       => $request->edo_osteometabolico,
                'deriv_vit_d'               => $request->deriv_vit_d,
                'dep_urea'                  => $request->dep_urea,
                'edo_nutricional'           => $request->edo_nutricional,
                'acc_vascular'              => $request->acc_vascular,
                'plan_terapeutico'          => $request->plan_terapeutico,
                'urgencia_hospitalizacion'  => $request->urgencia_hospitalizacion,
                'resuelto'                  => $request->resuelto,
                'motivo_consulta'           => $request->motivo_consulta,
                'evaluacion'                => $request->evaluacion,
                'solicitud_rx'              => $request->solicitud_rx,
                'progragrama_tx'            => $request->progragrama_tx,
                
                'fecha_informe'             => date('Y-m-d H:i:s', strtotime($request->fecha_informe)),

            ]);

        alert()->success('Se registró el informe satisfactoriamente. <br> para imprimir haga click <a href="'.route('impresion.infEvolucion', $informe->id).'" target="_BLANK">aqui</a>', 'Guardado')->html()->persistent("Aceptar");
        return back();
    }
}
