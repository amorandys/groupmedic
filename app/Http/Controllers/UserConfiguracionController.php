<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Auth;

class UserConfiguracionController extends Controller
{
    public function cambioSucursal(Request $request)
    {
		$usuario = User::find(Auth::user()->id);

		$configuracion = $usuario->configuracion;

		if ($configuracion) {
			$configuracion->sucursal_id = $request->sucursal_id;
			$configuracion->save();
		}

		$usuario->configuracion()->firstOrCreate(['sucursal_id' => $request->sucursal_id]);;

		alert()->info('Se modifico la sucursal actual', 'Completado');
		return back();
    }
}
