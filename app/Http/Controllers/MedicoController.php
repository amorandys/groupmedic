<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MedicosDataTable;

use App\Medico;

use Auth;

class MedicoController extends Controller
{
    public function index(MedicosDataTable $dataTable)
    {
        $medicos = Medico::all();
        
        return $dataTable->render('medico.index', [
                'medicos' => $medicos,
            ]);
    }

    public function create()
    {
        return view('medico.create');
    }

    public function store(Request $request)
    {
    	return $request->all();
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $medico = Medico::find($id);
        return view('medico.edit', ['medico' => $medico]);
    }

    public function update(Request $request, $id)
    {
        $medico = Medico::find($id);
        
        $medico->fill([
                'nombres'           => $request->nombres,
                'apellidos'         => $request->apellidos,
                'rut'               => str_replace('.', '', $request->rut),
                'rut_v'             => digitoVerificador($request->rut),
                'fecha_nacimiento'  => $request->fecha_nacimiento,
                'sexo'              => $request->sexo,
            ]);

        $medico->contacto->fill([
                'telefono'  => $request->telefono,
                'celular'   => $request->celular,
                'email'     => $request->email,
                'direccion' => $request->direccion,
                'region_id' => $request->region_id,
                'comuna_id' => $request->comuna_id,
            ]);

        alert()->success('Los datos del médico <br><strong>"'.$medico->nombres.', '.$medico->apellidos.'"</strong>,<br> han sido actualizados satisfactoriamente', '¡Actualizado!')->html()->persistent('Aceptar');
        return back();
    }

    public function destroy($id)
    {
    	
    }
}
