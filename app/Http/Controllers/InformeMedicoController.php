<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sucursal;
use App\Paciente;
use App\InfMedico;

use Auth;

class InformeMedicoController extends Controller
{
	public function index()
	{
		$sucursales = Sucursal::pluck('nombre', 'id');
		return view('ficham.informe.index', ['sucursales' => $sucursales]);
	}

	public function create()
	{

	}

	public function store(Request $request)
	{
		$paciente = Paciente::where('rut', str_replace('.', '', $request->rut_paciente))
			->where('rut_v', digitoVerificador($request->rut_paciente))
			->first();
		
		$medico = Auth::user()->medico;
		
		if (!$medico) {
            alert()->error('El usuario no esta asociado a un médico, no puede realizar esta operación', 'Error')->persistent('Aceptar');
            return back()->withInput();
        }

		$informe = InfMedico::firstOrCreate([
								'medico_id' 	=> $medico->id,
								'paciente_id' 	=> $paciente->id,
								'sucursal_id' 	=> Auth::user()->configuracion->sucursal->id,

								'diagnostico' 	=> nl2br($request->diagnostico),
								'tratamiento' 	=> nl2br($request->tratamiento),
								'observaciones' => nl2br($request->observaciones),
								'fecha_informe' => date('Y-m-d H:i:s', strtotime($request->fecha_informe)),
							]);
		
		alert()->success('Se registró el informe satisfactoriamente. <br> para imprimir haga click <a href="'.route('impresion.infMedico', $informe->id).'" target="_BLANK">aqui</a>', 'Guardado')->html()->persistent("Aceptar");
		return back();
		
	}
}
