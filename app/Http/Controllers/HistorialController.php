<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Paciente;
use App\InfEvoMedica;

class HistorialController extends Controller
{
    public function index()
    {

    }

    public function show($id)
    {
    	$paciente = Paciente::find($id);

    	return view('paciente.historial.index', ['paciente' => $paciente]);
    }

    public function mostrarEvolucion($id)
    {
    	$informe = InfEvoMedica::find($id);
    	$informe->medico;
    	$informe->paciente;
    	$informe->sucursal;

    	return response()->json($informe);
    }
}
