<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsuarioRequest;

use App\User;
use App\Role;
use App\Region;
use App\Sucursal;
use App\UserSucursal;

use DB;

class UserController extends Controller
{
	public function index()
	{

		return view('auth.index', ['usuarios' => User::simplePaginate(5)]);
	}

	public function create()
	{
		return view('auth.register', [
			'sucursales' 	=> Sucursal::all(),
			'regiones' 		=> Region::orderBy('descripcion')->where('pais_id', 81)->pluck('descripcion', 'id'),
			'role' 			=> Role::orderBy('display_name')->pluck('display_name', 'id')
			]);
	}

	public function store(UsuarioRequest $request)
	{
		$u_nombre = explode(" ",trim($request->nombres)); 
		$u_apellido = explode(" ",trim($request->apellidos)); 

		$usuario = User::firstOrCreate([
			'usuario' 	=> ($request->usuario) ?? $u_nombre[0].' '.$u_apellido[0],
			'login' 	=> $request->login,
			'email' 	=> $request->email,
			'tipo'		=> $request->tipo,
			]);

		if ($request->sucursal) {
			foreach ($request->sucursal as $key => $sucursal) {
				if ($sucursal) {
					$usuario->sucursales()->firstOrCreate(['sucursal_id' => $sucursal]);
				}
			}
		}

		$role = Role::find($usuario->tipo);
		$usuario->roles()->sync([$request->tipo]);

		switch ($request->tipo) {
			case 2:
				$medico = $usuario->medico()->firstOrCreate([
				    'nombres' 			=> $request->nombres,
				    'apellidos' 		=> $request->apellidos,
				    'rut' 				=> str_replace('.', '', $request->rut),
				    'rut_v' 			=> digitoVerificador($request->rut),
				    'fecha_nacimiento' 	=> date('Y-m-d', strtotime($request->fecha_nacimiento)),
				    'sexo' 				=> $request->sexo,
				    //'especialidad_id' 	=> 'prueba',
				]);

				$medico->contacto()->firstOrCreate([
					'telefono' 	=> $request->telefono,
					'celular' 	=> $request->celular,
					'email' 	=> $request->email,
					'region_id' => $request->region_id,
					'comuna_id' => $request->comuna_id,
					]);
				break;
			
			default:
				break;
		}


		
		switch ($request->tipo) {
			case 2:
				$mensaje = 'Usuario registrado en el sistema, datos afiliados al médico.';
				break;
			
			default:
				$mensaje = 'Usuario registrado.';
				break;
		}

		alert()->success($mensaje, '¡Guardado!')->persistent('Aceptar');
		return redirect()->route('usuario.index');
		return response()->json('success');
	}

	public function edit($id)
	{
		return view('auth.edit', [
				'usuario' 		=> User::find($id),
				'sucursales' 	=> Sucursal::all(),
				'regiones' 		=> Region::orderBy('descripcion')->where('pais_id', 81)->pluck('descripcion', 'id'),
				'role' 			=> Role::orderBy('display_name')->pluck('display_name', 'id')
			]);
	}

	public function update(Request $request, $id)
	{
		$usuario = User::find($id);
		$usuario->fill($request->all());
		$usuario->save();

		$usuario->sucursales()->delete();

		foreach ($request->sucursal as $key => $sucursal) {
			$usuario->sucursales()->create(['sucursal_id' => $sucursal]);
		}

		$role = Role::find($usuario->tipo);
		$usuario->roles()->sync([$request->tipo]);


		alert()->basic('Los datos del usuario "'.$usuario->usuario.'" se actualizaron satisfactoriamente.', '¡Bien hecho!')->persistent('Aceptar');
		return redirect()->route('usuario.index');

		return $request->all();
	}
}
