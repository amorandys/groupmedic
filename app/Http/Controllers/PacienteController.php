<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PacienteRequest;
use App\DataTables\PacientesDataTable;

use App\Pais;
use App\Region;
use App\Ciudad;
use App\Paciente;
use App\PacienteContacto;
use App\PacienteDiagnostico;

use DB;

class PacienteController extends Controller
{
    public function index(PacientesDataTable $dataTable)
    {
        return $dataTable->render('paciente.index');

    	return view('paciente.index', [
            'pacientes' => Paciente::paginate(12),
            ]);
    }

    public function create()
    {
    	return view('paciente.create', [
            'paises' => Pais::orderBy('descripcion')->pluck('descripcion', 'id'),
            ]);
    }

    public function store(PacienteRequest $request)
    {
        
        $paciente = Paciente::firstOrCreate([
                    'rut'                   => str_replace('.', '', $request->rut),
                    'rut_v'                 => digitoVerificador($request->rut),
                    'nombres'               => $request->nombres,
                    'apellidos'             => $request->apellidos,
                    'pais_nacimiento_id'    => $request->pais_nacimiento_id,
                    'pais_nacionalidad_id'  => $request->pais_nacionalidad_id,
                    'sexo'                  => $request->sexo,
                ]);
        
        $contacto  = new PacienteContacto($request->only(['telefono', 'celular', 'email', 'direccion']));
        $paciente->contacto()->save($contacto);
        
        //$diagnostico    = new PacienteDiagnostico($request->only(['sintomas', 'diagnostico']));
        //$paciente->diagnostico()->save($diagnostico);

        alert()->success('se registró la información del paciente '. $paciente->nombres.', '.$paciente->apellidos, '¡Completado!')->persistent('Aceptar');
        return redirect()->route('paciente.index');
    }

    public function show($id)
    {
        $paciente = DB::select('CALL paciente_info('.$id.')');
        $paciente = collect($paciente)->first();
        return response()->json($paciente);
        
        $paciente = Paciente::find($id);
        $paciente = collect($paciente)->merge($paciente->contacto);
    }

    public function edit($id)
    {
        $paciente = Paciente::find($id);
        $paises = Pais::orderBy('descripcion')->pluck('descripcion', 'id');

        /*
        $pacienteInfo = collect($paciente)->toArray();
        $pacienteInfo = (collect($paciente->contacto)->isEmpty()) ? $pacienteInfo : $pacienteInfo+collect($paciente->contacto)->toArray();
        */

        return view('paciente.edit', ['paciente' => $paciente, 'paises' => $paises]);
    }

    public function update(Request $request, $id)
    {
        #dd($request->all());

        $paciente = Paciente::find($id);
        $paciente->fill([
                'rut'                   => str_replace('.', '', $request->rut),
                'rut_v'                 => digitoVerificador($request->rut),
                'nombres'               => $request->nombres,
                'apellidos'             => $request->apellidos,
                'sexo'                  => $request->sexo,
                'edo_civil'             => $request->edo_civil,
                'pais_nacimiento_id'    => $request->pais_nacimiento_id,
                'pais_nacionalidad_id'  => $request->pais_nacionalidad_id
            ]);

        $paciente->contacto->fill([
                'telefono' => $request->telefono,
                'celular' => $request->celular,
                'email' => $request->email,
                'direccion' => $request->direccion,
            ]);

        alert()->basic('Los datos del paciente <strong>'.$paciente->nombres.' '.$paciente->apellidos.'</strong> han sido actualizados satisfactoriamente.', 'Actualizado')->html()->persistent('Aceptar');
        return redirect()->route('paciente.index');
    }

    public function autocomplete(Request $request)
    {

        return Paciente::search($request->get('q'))->get();

        $data = Paciente::select("nombres")
                        ->where('nombres', 'LIKE', "%{$request->input('query')}%")
                        ->get();
        return response()->json($data);
    }
}
