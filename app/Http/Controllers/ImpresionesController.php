<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InfMedico;
use App\InfNefrologia;
use App\InfEvoMedica;


use PDF;

class ImpresionesController extends Controller
{
    public function infMedico($id)
    {
        

    	$informe = InfMedico::find($id);
    	
    	$view = view('impresiones.informes.informe_medico', ['informe' => $informe])->render();
        
        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view)->setPaper('letter');
        
        return $pdf->stream();
    }

    public function infNefrologico($id)
    {
    	$informe = InfNefrologia::find($id);

    	$view = view('impresiones.informes.informe_nefrologico', ['informe' => $informe])->render();
        
        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view)->setPaper('letter');
        
        return $pdf->stream();
    }

    public function infEvolucion($id)
    {
        $informe = InfEvoMedica::find($id);

        $view = view('impresiones.informes.informe_evolucion', ['informe' => $informe])->render();
        
        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view)->setPaper('letter');
        
        return $pdf->stream();
    }
}
