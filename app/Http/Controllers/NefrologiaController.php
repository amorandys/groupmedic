<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sucursal;
use App\InfNefrologia;
use App\Paciente;

use Auth;

class NefrologiaController extends Controller
{
    public function index()
    {
        $sucursales = Sucursal::pluck('nombre', 'id');
    	return view('ficham.nefrologia.index', ['sucursales' => $sucursales]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        //dd($request->all());

        $paciente = Paciente::where('rut', str_replace('.', '', $request->rut_paciente))
            ->where('rut_v', digitoVerificador($request->rut_paciente))
            ->first();
        
        $medico = Auth::user()->medico;
        
        if (!$medico) {
            alert()->error('El usuario no esta asociado a un médico, no puede realizar esta operación', 'Error')->persistent('Aceptar');
            return back()->withInput();
        }
        
        $informe = InfNefrologia::firstOrCreate([
                'medico_id'                 => 1,
                'paciente_id'               => $paciente->id,
                'sucursal_id'               => Auth::user()->configuracion->sucursal->id,
                'diagnostico'               => $request->diagnostico,
                'd_peso'                    => $request->d_peso,
                'd_qb'                      => $request->d_qb,
                'd_qtipo'                   => $request->d_qtipo,
                'd_flujo'                   => $request->d_flujo,
                'd_filtro'                  => $request->d_filtro,
                'd_heparina'                => $request->d_heparina,
                'd_tipo_acceso_vascular'    => $request->d_tipo_acceso_vascular,
                'd_k'                       => $request->d_k,
                'd_ca'                      => $request->d_ca,
                'd_bicarbonato'             => $request->d_bicarbonato,
                'l_hto'                     => $request->l_hto,
                'l_saturacion'              => $request->l_saturacion,
                'l_ferritina'               => $request->l_ferritina,
                'l_ktv'                     => $request->l_ktv,
                'l_bun'                     => $request->l_bun,
                'l_creatinina'              => $request->l_creatinina,
                'l_albumina'                => $request->l_albumina,
                'l_ast'                     => $request->l_ast,
                'l_k'                       => $request->l_k,
                'l_ca'                      => $request->l_ca,
                'l_p'                       => $request->l_p,
                'l_falcalina'               => $request->l_falcalina,
                'l_pth'                     => $request->l_pth,
                'l_hiv'                     => $request->l_hiv,
                'l_vhb'                     => $request->l_vhb,
                'l_vhc'                     => $request->l_vhc,
                'tratamiento'               => $request->tratamiento,
                'observaciones'             => $request->observaciones,
                'fecha_informe'             => date('Y-m-d H:i:s', strtotime($request->fecha_informe)),
            ]);

        alert()->success('Se registró el informe satisfactoriamente. <br> para imprimir haga click <a href="'.route('impresion.infNefrologico', $informe->id).'" target="_BLANK">aqui</a>', 'Guardado')->html()->persistent("Aceptar");
        return back();
    }
}
