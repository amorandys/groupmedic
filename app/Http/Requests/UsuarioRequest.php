<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo'              => 'required',
            'nombres'           => 'filled',
            'apellidos'         => 'filled',
            'usuario'           => 'filled',
            'login'             => 'required',
            'email'             => 'required|email',

            'rut'               => 'filled',
            'fecha_nacimiento'  => 'filled',
            'sexo'              => 'filled',
            'telefono'          => 'filled',
            'celular'           => 'filled',
            'region_id'         => 'filled',
            'comuna_id'         => 'filled',
            'direccion'         => 'filled',
        ];
    }

    public function messages()
    {
        return [
            '*.required'        => 'información obligatoria',
            '*.filled'          => 'información obligatoria',
            '*.email'           => 'no es un correo válido'
        ];
    }
}
