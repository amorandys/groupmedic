<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // INFORMACIÓN PERSONAL
            'rut'                   => 'required|root_paciente',
            'nombres'               => 'required',
            'apellidos'             => 'required',
            'pais_nacimiento_id'    => 'required',
            'pais_nacionalidad_id'  => 'required',

            // INFORMACIÓN DE CONTACTO
            'telefono'              => 'required_without:celular',
            'celular'               => 'required_without:telefono',
            'email'                 => 'required|email',
            'direccion'             => 'required',
            
            
            //'' => '',
        ];
    }

    public function messages()
    {
        return [
            '*.required'    => 'este campo es obligatorio',
            '*.email'       => 'email no valido',
        ];
    }
}
