<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EvolucionMedicaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut_paciente'  => 'required',
            'sucursal_id'   => 'required',
            'fecha'         => 'required',

            'estado'        => 'required',
            'tolerancia_hd' => 'required',
        ];
    }
}
