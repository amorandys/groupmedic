<?php

use App\Sucursal;


function cboSucursales()
{
	$sucursales = Sucursal::join('user_sucursales', 'sucursales.id', '=', 'user_sucursales.sucursal_id')
                    ->orderBy('sucursales.nombre')
                    ->where('user_sucursales.user_id', Illuminate\Support\Facades\Auth::user()->id)
                    ->pluck('sucursales.nombre', 'sucursales.id');

	    if (count($sucursales)>0) {
    	return $sucursales;
    }
    else
    {
    	return [];
    }
}

function digitoVerificador($rut)
{
	$rut = str_replace('.', '', $rut); //Elimino todos los valores no numericos

	$rut = strrev($rut); // Invierto el rut
	$prueba = str_split($rut); // lo convierto en un arreglo para poder recorrerlo

	$i = 2;
	$acumulador = 0;
	foreach ($prueba as $value) {
		$acumulador = $acumulador + ($value*$i); // Se multiplica cada digito por el valor de la serie correspondiente, y se va acumulando en una variable el resultado
		($i <= 6) ? $i++ : $i=2; // Si se alcanza el ultimo digito de la serie se reinicia el contador
	}
	
	$modulo = floor($acumulador/11); // Se divide entre 11 y redondeo el numero hacia abajo
	$resto = ($acumulador-(11 * $modulo));
	$resto = (11 - $resto);
	switch ($resto) {
		case 11:
			$digito_verificador = 0;
			break;
		case 10:
			$digito_verificador = 'k';
			break;
		default:
			$digito_verificador = $resto;
			break;
	}

	return $digito_verificador;

}
