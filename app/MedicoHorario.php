<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicoHorario extends Model
{
    protected $table = 'medico_horarios';

    protected $guarded = [];
}
