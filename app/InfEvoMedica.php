<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfEvoMedica extends Model
{
    protected $table = 'inf_evo_medica';

    protected $guarded = [];

    public function medico()
    {
    	return $this->belongsTo('App\Medico');
    }

    public function paciente()
    {
    	return $this->belongsTo('App\Paciente');
    }

    public function sucursal()
    {
        return $this->belongsTo('App\Sucursal');
    }
}
