<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteDiagnostico extends Model
{
    protected $table = 'paciente_diagnosticos';

    protected $guarded = [];
}
