<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConfiguracion extends Model
{
    protected $table = 'user_configuracion';

    protected $guarded = [];

    public function sucursal()
    {
    	return $this->belongsTo('App\Sucursal');
    }

}
