<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;


use Carbon\Carbon;

use Auth;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Blade::directive('submit_button', function ($expression) {
            $boton = "<button type='submit' class='btn btn-flat border-primary text-primary btn-primary'><i class='icon-profile position-left'></i> ".$expression."</button>";
            return $boton;
        });

        Blade::directive('imprimir', function ($expresion){
            $boton = '<a href="'.route($expresion, ':ID').'" target="_BLANK" class="btn btn-primary">Imprimir informe</a>';
            return $boton;
        });

        Blade::directive('edad', function ($expresion) {
            list($y,$m,$d) = explode("-", $expresion);
            $edad = Carbon::createFromDate($y, $m, $d)->age;
            return $edad;
        });

       
        View::share('configuracionUsuario', Auth::user());
    }

    public function register()
    {
        //
    }
}
