<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

use App\Paciente;
use App\Medico;

class ValidacionesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //VALIDAR RUT DE PACIENTE
        Validator::extend('root_paciente', function ($attribute, $value, $parameters, $validator) {
            $rut = $rut = str_replace('.', '', $value);
            $paciente = Paciente::where('rut', $rut)->first();
            if (count($paciente)>0) {
                return false;
            }
            return true;
        });

        //VALIDAR RUT DE MEDICO
        Validator::extend('root_medico', function ($attribute, $value, $parameters, $validator) {
            $rut = $rut = str_replace('.', '', $value);
            $paciente = Medico::where('rut', $rut)->first();
            if (count($paciente)>0) {
                return false;
            }
            return true;
        });

        /*
        //VALIDAR RUT DE ENFERMERA
        Validator::extend('root_enfermera', function ($attribute, $value, $parameters, $validator) {
            $rut = $rut = str_replace('.', '', $value);
            $paciente = Medico::where('rut', $rut)->first();
            if (count($paciente)>0) {
                return false;
            }
            return true;
        });
        */
    }

    public function register()
    {

    }
}
