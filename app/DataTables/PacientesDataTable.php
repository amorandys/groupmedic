<?php

namespace App\DataTables;

use App\Paciente;
use Yajra\Datatables\Services\DataTable;

class PacientesDataTable extends DataTable
{
    public function ajax()
    {
        $paciente = $this->query();
        return $this->datatables
        ->eloquent($this->query())
        ->editColumn('rut', function ($paciente) {
            return number_format($paciente->rut, 0, '', '.').'-'.$paciente->rut_v;
        })
        ->editColumn('action', function ($paciente) {
            $acciones  = '';

            $acciones .= '<div class="btn-group">';
            $acciones .= '<button type="button" class="btn btn-xs btn-flat border-slate text-slate-700  btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
            $acciones .= '<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>';
            $acciones .= '</button>';
            $acciones .= '<ul class="dropdown-menu dropdown-menu-right">';
            $acciones .= '<li><a href="#" onclick="pacienteInfo('.$paciente->id.')"><i class="icon-eye"></i> Ver información</a></li>';
            $acciones .= '<li><a href="'.route('paciente.edit', $paciente->id).'"><i class="icon-pencil6"></i> Editar</a></li>';
            $acciones .= '<li><a href="'.route('historial.show', $paciente->id).'"><i class="icon-profile"></i> Historial del paciente</a></li>';
            $acciones .= '</ul>';
            $acciones .= '</div>';

            
        return $acciones;
    })
        ->make(true);
    }

    public function query()
    {
        $pacientes = Paciente::select('id', 'rut', 'rut_v', 'nombres', 'apellidos', 'sexo', 'created_at', 'updated_at');
        return $this->applyScopes($pacientes);
    }

    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->ajax('')
        ->addAction(['width' => '80px', 'title' => 'Acciones'])
        ->parameters($this->getBuilderParameters());
    }

    protected function getColumns()
    {
        return [
        'rut'       => ['title' => 'RUT'],
        'nombres'   => ['title' => 'Nombres'],
        'apellidos' => ['title' => 'Apellidos'],
        'sexo'      => ['title' => 'Sexo']
        ];
    }

    protected function getBuilderParameters()
    {
        return [
        "processing"    => false,
        "serverSide"    => true,
        "order"         => [ 0, "asc" ],
        "lengthChange"  => false,
        "searching"     => true,
        "ordering"      => true,
        "autoWidth"     => true,
        "paging"        => true,
        "info"          => false,
        "language"      => ["url" => "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"],
        "stateSave"     => true,
        "info"          => true,
        "buttons"       => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
        ];
    }

    protected function filename()
    {
        return 'pacientes_data_' . time();
    }
}
