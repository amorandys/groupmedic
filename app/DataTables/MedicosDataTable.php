<?php

namespace App\DataTables;

use App\Medico;
use Yajra\Datatables\Services\DataTable;

class MedicosDataTable extends DataTable
{
    public function ajax()
    {
        $medico = $this->query();
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('rut', function ($medico) {
                return number_format($medico->rut, 0, '', '.').'-'.$medico->rut_v;
            })
            ->editColumn('action', function ($medico) {
                $acciones  = '';

            $acciones .= '<div class="btn-group">';
            $acciones .= '<button type="button" class="btn btn-xs btn-flat border-slate text-slate-700  btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
            $acciones .= '<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>';
            $acciones .= '</button>';
            $acciones .= '<ul class="dropdown-menu dropdown-menu-right">';
            $acciones .= '<li><a href="#"><i class="icon-eye"></i> Ver información</a></li>';
            $acciones .= '<li><a href="'.route('horario.show', $medico->id).'"><i class="icon-calendar"></i> Agenda médica</a></li>';
            $acciones .= '</ul>';
            $acciones .= '</div>';

                return $acciones;
            })
            ->make(true);
    }

    public function query()
    {
        $query = Medico::query();

        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px', 'title' => 'Acciones'])
                    ->parameters($this->getBuilderParameters());
    }

    protected function getColumns()
    {
        return [
            'rut'       => ['title' => 'RUT'],
            'nombres'   => ['title' => 'Nombres'],
            'apellidos' => ['title' => 'Apellidos'],
            'sexo'      => ['title' => 'Sexo']
        ];
    }

    protected function getBuilderParameters()
    {
        return [
        "processing"    => false,
        "serverSide"    => true,
        "order"         => [ 0, "asc" ],
        "lengthChange"  => false,
        "searching"     => true,
        "ordering"      => true,
        "autoWidth"     => true,
        "paging"        => true,
        "info"          => false,
        "language"      => ["url" => "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"],
        "stateSave"     => true,
        "info"          => true,
        "buttons"       => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
        ];
    }

    protected function filename()
    {
        return 'medicosdatatables_' . time();
    }
}
