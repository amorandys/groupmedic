<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    protected $fillable = [
        'usuario', 'email', 'password', 'login', 'tipo'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function medico()
    {
        return $this->hasOne('App\Medico');
    }

    public function sucursales()
    {
        return $this->hasMany('App\UserSucursal');
    }

    public function configuracion()
    {
        return $this->hasOne('App\UserConfiguracion');
    }
}
