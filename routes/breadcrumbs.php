<?php

// Home
Breadcrumbs::register('index', function($breadcrumbs)
{
    $breadcrumbs->push('Inicio', route('index'));
});

// ------------------------ PACIENTES ------------------------
Breadcrumbs::register('paciente.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Pacientes', route('paciente.index'));
});

Breadcrumbs::register('paciente.create', function($breadcrumbs)
{
    $breadcrumbs->parent('paciente.index');
    $breadcrumbs->push('Cargar información', route('paciente.create'));
});

Breadcrumbs::register('paciente.edit', function ($breadcrumbs, $paciente)
{
    $breadcrumbs->parent('paciente.index');
    $breadcrumbs->push('Editar paciente', route('paciente.edit', $paciente));
});
// ------------------------ /PACIENTES -----------------------

// ------------------------ MEDICOS ------------------------
Breadcrumbs::register('medico.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Médicos', route('medico.index'));
});

Breadcrumbs::register('medico.create', function($breadcrumbs)
{
    $breadcrumbs->parent('medico.index');
    $breadcrumbs->push('Cargar información', route('medico.create'));
});

Breadcrumbs::register('medico.edit', function ($breadcrumbs, $medico)
{
    $breadcrumbs->parent('medico.index');
    $breadcrumbs->push('Editar medico', route('medico.edit', $medico));
});

// ------------------------ /MEDICOS -----------------------

// ------------------------ AGENDA ------------------------
Breadcrumbs::register('agenda.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Agenda', route('agenda.index'));
});
// ------------------------ /AGENDA -----------------------

// ------------------------ USUARIOS ------------------------
Breadcrumbs::register('usuario.index', function($breadcrumbs)
{
	$breadcrumbs->parent('index');
	$breadcrumbs->push('Usuarios', route('usuario.index'));
});

Breadcrumbs::register('usuario.create', function($breadcrumbs)
{
    $breadcrumbs->parent('usuario.index');
    $breadcrumbs->push('Registar', route('usuario.create')); 
});

Breadcrumbs::register('usuario.edit', function ($breadcrumbs, $usuario)
{
    $breadcrumbs->parent('usuario.index');
    $breadcrumbs->push('Editar usuario', route('usuario.edit', $usuario));
});

// ------------------------ /USUARIOS -----------------------


// ------------------------ ROLES ------------------------
Breadcrumbs::register('roles.index', function ($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Roles', route('roles.index'));
});

Breadcrumbs::register('roles.edit', function ($breadcrumbs, $roles)
{
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('Editar roles', route('roles.edit', $roles));
});
// ------------------------ /ROLES ------------------------


// ------------------------ SUCURSALES ------------------------
Breadcrumbs::register('sucursal.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Sucursales', route('sucursal.index'));
});
// ------------------------ /SUCURSALES -----------------------


// ------------------------ INFORME MÉDICO ------------------------
Breadcrumbs::register('informe.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Informe', route('informe.index'));
});
// ------------------------ /INFORME MÉDICO -----------------------


// ------------------------ NEFROLOGÍA ------------------------
Breadcrumbs::register('nefrologia.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Nefrología', route('nefrologia.index'));
});
// ------------------------ /NEFROLOGÍA -----------------------


// ------------------------ EVOLUCIÓN ------------------------
Breadcrumbs::register('evolucion.index', function($breadcrumbs)
{
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Evolución', route('evolucion.index'));
});
// ------------------------ /EVOLUCIÓN -----------------------


// ------------------------ HISTORIAL DEL PACIENTE ------------------------
Breadcrumbs::register('historial.show', function($breadcrumbs, $paciente)
{
    $breadcrumbs->parent('paciente.index');
    $breadcrumbs->push('Historial', route('historial.show', $paciente));
});
// ------------------------ /HISTORIAL DEL PACIENTE -----------------------


// ------------------------ HORARIO DEL MÉDICO ------------------------

Breadcrumbs::register('horario.show', function($breadcrumbs, $medico)
{
    $breadcrumbs->parent('medico.index');
    $breadcrumbs->push('Horario', route('horario.show', $medico));
});
// ------------------------ /HORARIO DEL MÉDICO -----------------------
