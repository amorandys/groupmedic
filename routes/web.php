<?php

use App\Region;
use App\Comuna;


Route::group(['middleware' => ['web']], function () {
    
    Route::get('/login', function () {
        return view('auth/login');
    })->name('login');

	Route::post('/login', 'Auth\LoginController@login');
});


Route::group(['middleware' => ['auth', 'web']], function () {
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('/', function () {
	    return view('welcome');
	})->name('index');

	Route::get('find', 'SearchController@find');
	
	Route::get('paciente/search', 'PacienteController@autocomplete')->name('paciente.search');
	Route::resource('paciente', 'PacienteController');
	Route::resource('medico', 'MedicoController');
	Route::resource('usuario', 'UserController');
	Route::resource('roles', 'RolesController');
	Route::resource('agenda', 'AgendaController');
	Route::resource('sucursal', 'SucursalController');
	Route::resource('informe', 'InformeMedicoController');
	Route::resource('nefrologia', 'NefrologiaController');
	Route::resource('evolucion', 'EvolucionController');
	Route::resource('historial', 'HistorialController');
	Route::resource('horario', 'MedicoHorarioController');

	Route::get('historial/{id}/paciente/evolucion/mostrar', 'HistorialController@mostrarEvolucion')->name('mostrarEvolucion');

	Route::get('impresion/{id}/informe/medico', 'ImpresionesController@infMedico')->name('impresion.infMedico');
	Route::get('impresion/{id}/informe/nefrologia', 'ImpresionesController@infNefrologico')->name('impresion.infNefrologico');
	Route::get('impresion/{id}/informe/evolucion', 'ImpresionesController@infEvolucion')->name('impresion.infEvolucion');

	Route::post('configuracion/usuario/sucursal', 'UserConfiguracionController@cambioSucursal')->name('usuario.configuracion.sucursal');

	Route::get('paciente/{id}/comprobar', function($rut) {
	    $paciente = \App\Paciente::where('rut', str_replace('.', '', $rut))->where('rut_v', digitoVerificador($rut))->first();
	    if (count($paciente)>0) {
	    	return response()->json(['paciente' => $paciente, 'result' => true]);
	    }
	    return response()->json(['paciente' => [], 'result' => false]);
	})->name('paciente.comprobar');

	Route::get('global/regiones/{pais_id}', function ($pais_id) {
		return Region::where('pais_id', $pais_id)->orderBy('descripcion')->pluck('descripcion', 'id');
	})->name('global.regiones');

	Route::get('global/comunas/{region_id}', function ($region_id) {
		return Comuna::where('region_id', $region_id)->orderBy('descripcion')->pluck('descripcion', 'id');
	})->name('global.comunas');

	Route::get('digito/verificador/{rut}', function($rut){
		$digito = digitoVerificador($rut);
		return response()->json($digito);
	})->name('digito.verificador');

});