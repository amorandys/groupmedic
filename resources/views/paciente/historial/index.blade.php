@extends('layouts.app')

@push('js')
{!! Html::script('js/historial-index.js') !!}
@endpush

@section('page-title', 'Informes del paciente')

@section('content')

<div class="alert alert-info alert-styled-left alert-bordered">
	<span class="text-semibold display-block">{{ $paciente->nombres.' '.$paciente->apellidos }}</span>
	Puede visualizar todos los informes realizados a este paciente organizados cronológicamente.
</div>

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-white border-top-teal border-top-lg">
			<div class="panel-heading">
				<h5 class="panel-title">Informes médicos</h5>
			</div>
			<div class="list-group no-border">
				@foreach (collect($paciente->infMedico)->sortByDesc('fecha_informe') as $element)
				<a href="#" class="list-group-item">
					<i class="icon-file-pdf"></i> {{ $element->fecha_informe }} <span class="label text-default"><i class="icon-circle-right2"></i></span>
				</a>
				@endforeach

			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-white border-top-teal border-top-lg">
			<div class="panel-heading">
				<h5 class="panel-title">Informes nefrológicos</h5>
			</div>
			<div class="list-group no-border">
				@foreach (collect($paciente->infNefrologico)->sortByDesc('fecha_informe') as $element)
				<a href="#" class="list-group-item">
					<i class="icon-file-pdf"></i> {{ $element->fecha_informe }} <span class="label text-default"><i class="icon-circle-right2"></i></span>
				</a>
				@endforeach
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-white border-top-teal border-top-lg">
			<div class="panel-heading">
				<h5 class="panel-title">Evolución médica</h5>
			</div>
			<div class="list-group no-border">
				@foreach (collect($paciente->infEvoMedica)->sortByDesc('fecha_informe') as $element)
				<a href="#" class="list-group-item" onclick="mostrarEvolucion({{ $element->id }})">
					<i class="icon-file-pdf"></i> {{ $element->fecha_informe }} <span class="label text-default"><i class="icon-circle-right2"></i></span>
				</a>
				@endforeach
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="mdlEvolucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Informe Evolucion Médica del Paciente</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6 content-group">
						<br>
						<ul class="list-condensed list-unstyled">
							<li id="medico"></li>
							<li id="sucursal"></li>
						</ul>
					</div>

					<div class="col-sm-6 content-group">
						<div class="invoice-details">
							<h6 class="text-uppercase text-semibold" id="folio"></h6>
							<ul class="list-condensed list-unstyled">
								<li id="fecha_informe"></li>
							</ul>
						</div>
					</div>
				</div>
				<fieldset>
					<legend>Información general<i class="icon-file-text2 pull-right"></i></legend>
					<div class="content-group">
						<ul class="list-condensed list-unstyled invoice-payment-details">
							<li>Estado general: <span class="text-semibold" id="estado"></span></li>
							<li>Tolerancia a la HD: <span class="text-semibold" id="tolerancia_hd"></span></li>
							<li>Complicaciones Intradialisis: <span class="text-semibold" id="compli_intra"></span></li>
							<li>Causa: <span class="text-semibold" id="causa_compli_intra"></span></li>
							<li>Llega al peso seco: <span class="text-semibold" id="peso_seco"></span></li>
						</ul>
					</div>
					
				</fieldset>

				<fieldset>
					<legend>Complicaciones cardiovasculares<i class="icon-heart-broken2 pull-right"></i></legend>
					<div class="content-group">
						<ul class="list-condensed list-unstyled invoice-payment-details">
							<li>Complicaciones cardiovasculares: <span class="text-semibold" id="compli_cardio"></span></li>
							<li>Arritmias (causa): <span class="text-semibold" id="arritmia_causa"></span></li>
							<li>Estado ematológico: <span class="text-semibold" id="edo_hermatologico"></span></li>
							<li>Transfusiones (Cantidad):<span class="text-semibold" id="transfusion_cantidad"></span></li>
							<li>Estado Osteometabolico / Acido-Base:<span class="text-semibold" id="edo_osteometabolico"></span></li>
							<li>Depuración Urea:<span class="text-semibold" id="dep_urea"></span></li>
							<li>Acceso vascular:<span class="text-semibold" id="acc_vascular"></span></li>
							<li>Usa derivados Vit-D (Tipo/Dosis):<span class="text-semibold" id="deriv_vit_d"></span></li>
							<li>Estado nutricional:<span class="text-semibold" id="edo_nutricional"></span></li>
							<li>Plan Terapeutico:<span class="text-semibold" id="plan_terapeutico"></span></li>
							<li>Consulta Urgencias/Hospitalización:<span class="text-semibold" id="urgencia_hospitalizacion"></span></li>
							<li>Resuelto<span class="text-semibold" id="resuelto"></span></li>
						</ul>
					</div>
				</fieldset>
				<hr>
				{{--

				
				hierro
				
				motivo_consulta
				
				
				progragrama_tx
				
				solicitud_rx
				
				
				
				
				
				usa_epo_dosis
								--}}
			</div>
			<div class="modal-footer">
				<a href="#" target="_BLANK" class="btn btn-flat border-primary text-primary btn-sm inprmirInfEvolucion"><i class="icon-printer position-left"></i> Imprimir</a>
			</div>
			
		</div>
	</div>
</div>

<a href="{{ route('mostrarEvolucion', ':ID') }}" class="mostrarEvolucion hide"></a>

<a href="{{ route('impresion.infMedico', ':ID') }}" class="hide infMedico"></a>
<a href="{{ route('impresion.infNefrologico', ':ID') }}" class="hide infNefrologico"></a>
<a href="{{ route('impresion.infEvolucion', ':ID') }}" class="hide infEvolucion"></a>
@endsection