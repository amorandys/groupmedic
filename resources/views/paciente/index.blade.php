@extends('layouts.app')

@section('page-title', 'Maestro de Pacientes')

@push('css')
@endpush

@push('js-files')
{!! Html::script('assets/js/plugins/tables/datatables/datatables.min.js') !!}
@endpush

@push('js')
{!! Html::script('js/paciente-index.js') !!}
{!! Html::script('https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js') !!}
{!! Html::script('/vendor/datatables/buttons.server-side.js') !!}

@endpush

@section('opciones')
<ul class="nav navbar-nav">
	<li>
		<a href="{{ route('paciente.create') }}" >
			<i class="icon-user-plus position-left"></i> Agregar paciente
		</a>
	</li>

</ul>
@endsection

@section('content')

<div class="panel panel-flat">
	<div class="panel-body">
		{!! $dataTable->table() !!}
	</div>
</div>


<div class="modal fade" id="mdPacienteInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><i class="icon-vcard position-left"></i>Información del paciente</h4>
			</div>
			<div class="modal-body">
				<address>
					<strong>Nombres y Apellidos:</strong><span class="pull-right" id="paciente"></span><br>
					<strong>RUT:</strong> <span class="pull-right" id="rut"></span><br>
					<strong>Sexo:</strong> <span class="pull-right" id="sexo"></span><br>
					<strong>Nacionalidad:</strong> <span class="pull-right" id="nacionalidad"></span><br>
					<strong>Pais de nacimiento:</strong> <span class="pull-right" id="pais_nacimiento"></span>
				</address>

				<address>
					<strong>Correo electrónico: </strong><span class="pull-right" id="email"></span><br>
					<strong>Teléfono: </strong><span class="pull-right" id="telefono"></span><br>
					<strong>Celular: </strong><span class="pull-right" id="celular"></span><br>
				</address>
			</div>
			
		</div>
	</div>
</div>
<a href="{{ route('paciente.show', ':ID') }}" class="paciente_info hidden"></a>

{!! $dataTable->scripts() !!}
@endsection
