@extends('layouts.app')

@section('page-title', $paciente->nombres.' '.$paciente->apellidos)

@section('content')
{!! Form::model($paciente->toArray()+$paciente->contacto->toArray(), ['route' => ['paciente.update', $paciente->id], 'method' => 'PUT', 'autocomplete' => 'off', 'id' => 'frmCreatePaciente']) !!}
<div class="row">
	<div class="col-md-8 col-lg-offset-2">
		<div class="panel panel-flat border-top-lg border-top-primary">
			<div class="panel-heading">
				<h5 class="panel-title">Datos personales</h5>
			</div>
			<div class="panel-body">

				<div class="col-md-4">

					<div class="form-group  form-group-xs">
						{!! Form::label('rut', 'RUT', ['class' => '']) !!}
						<div class="input-group">
							{!! Form::text('rut', null, ['class' => 'form-control text-bold rut']) !!}
							<span class="input-group-addon">-&nbsp;&nbsp; </span>
						</div>

					</div>
				</div>


				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('nombres', '', ['class' => '']) !!}
						{!! Form::text('nombres', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('apellidos', 'Apellidos', ['class' => '']) !!}
						{!! Form::text('apellidos', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('sexo', '', ['class' => '']) !!}
						{!! Form::select('sexo', ['masculino' => 'Masculino', 'femenino' => 'Femenino'], null, ['class' => 'form-control text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('edo_civil', 'Estado Civil') !!}
						{!! Form::select('edo_civil', ['soltero' => 'Soltero', 'casado' => 'Casado'], null, ['class' => 'form-control text-bold', 'placeholder' => '']) !!}
					</div>
				</div>
				
				<div class="clearfix"></div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('pais_id', 'País de nacimiento') !!}
						{!! Form::select('pais_nacimiento_id', $paises, null, ['class' => 'form-control select2 text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('pais_id', 'Nacionalidad') !!}
						{!! Form::select('pais_nacionalidad_id', $paises, null, ['class' => 'form-control select2 text-bold', 'placeholder' => '']) !!}
					</div>
				</div>
				
			</div>
		</div>
		<div class="panel panel-flat border-top-lg border-top-primary">
			<div class="panel-heading">
				<h5 class="panel-title">Información de contacto</h5>
			</div>
			<div class="panel-body">
				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('telefono', '', ['class' => '']) !!}
						{!! Form::text('telefono', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('celular', '', ['class' => '']) !!}
						{!! Form::text('celular', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('email', '', ['class' => '']) !!}
						{!! Form::text('email', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group form-group-xs">
						{!! Form::label('direccion', 'Dirección', ['class' => '']) !!}
						{!! Form::textArea('direccion', null, ['class' => 'form-control text-bold', 'rows' => '2']) !!}
					</div>
				</div>


			</div>
		</div>

		<div class="panel">
			<div class="panel-body text-right">
				<button type="submit" class="btn border-slate text-slate-800 btn-flat legitRipple"><i class="icon-file-plus position-left"></i> Guardar información</button>
			</div>
		</div>
	</div>
</div>
@endsection