@extends('layouts.app')

@section('page-title', 'Agenda médica')

@push('js-files')
{!! Html::script('assets/js/plugins/ui/moment/moment.min.js') !!}
{!! Html::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
{!! Html::script('assets/js/plugins/ui/fullcalendar/lang/es.js') !!}
{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
@endpush

@push('js')
{!! Html::script('js/agenda-index.js') !!}
@endpush

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-flat border-top-purple-300 border-top-lg">
			<div class="panel-heading">
				<h5 class="panel-title">
					<i class="icon-file-text2 position-left"></i>
					Agregar cita al calendario
				</h5>
			</div>
			{!! Form::open(['route' => 'agenda.store']) !!}
			<div class="panel-body">
				<div class="collapse in" id="demo">

					<div class="form-group has-feedback has-feedback-left">
						<div class="input-group">
							{!! Form::text('rut', null, ['class' => 'form-control rut', 'placeholder' => 'Indique el rut del paciente']) !!}
							<span class="input-group-addon">-&nbsp;&nbsp; </span>
						</div>
						<div class="form-control-feedback">
							<i class="icon-user text-muted"></i>
						</div>
					</div>
					<div class="form-group has-feedback has-feedback-left">
						{!! Form::text('paciente_info', null, ['class' => 'form-control input-xs', 'id' => 'paciente_info', 'disabled' => 'disabled', 'placeholder' => 'Indique el rut del paciente']) !!}
						<div class="form-control-feedback">
							<i class="icon-user text-muted"></i>
						</div>
					</div>

					<div class="form-group has-feedback has-feedback-left">
						{!! Form::textArea('motivo', null, ['class' => 'form-control input-xs', 'placeholder' => 'Motivo de la consulta', 'rows' => 3]) !!}
						<div class="form-control-feedback">
							<i class="icon-office text-muted"></i>
						</div>
					</div>
					
					<div class="form-group has-feedback has-feedback-left">
						{!! Form::text('medico_id', null, ['class' => 'form-control input-xs', 'placeholder' => 'Médico']) !!}
						<div class="form-control-feedback">
							<i class="icon-office text-muted"></i>
						</div>
					</div>
					
					<div class="form-group has-feedback has-feedback-left">
						{!! Form::text('fecha_cita', null, ['class' => 'form-control input-xs', 'placeholder' => 'Fecha para la cita']) !!}
						<div class="form-control-feedback">
							<i class="icon-calendar text-muted"></i>
						</div>
					</div>		
				</div>
			</div>
			<div class="panel-footer text-right p-5">
				<button type="submit" class="btn btn-flat border-slate-300 text-slate"> Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-flat border-top-purple-300 border-top-lg">
			<div class="panel-body">
				<div class="calendario"></div>
			</div>
		</div>
	</div>
</div>

<a href="{{ route('paciente.comprobar', ':ID') }}" class="comprobar"></a>
@endsection