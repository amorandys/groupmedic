<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />
	{!! Html::style('css/reportes/bootstrap/bootstrap.css') !!}
	{!! Html::style('css/reportes/bootstrap/style.css') !!}

</head>
<body>
	<header>

		<table class="table no-border">
			<thead>
				<tr>
					<th width="20%">LOGO</th>
					<th>
						<h1 class="text-center">@yield('informe-titulo')</h1>
					</th>
					<th width="20%" class="text-right" style="vertical-align: top;">Folio: {{ str_pad($informe->id, 5, 0, STR_PAD_LEFT) }}</th>
				</tr>
			</thead>
		</table>
	</header>
	<main>
		@yield('content')
	</main>
	<footer>
		<table class="table no-border">
			<tr>
				<td class="text-center">
					Médico a cargo durante el mes (firma y timbre)
					<br><br>
					_____________________________________________________
				</td>
			</tr>
			<tr>
				<th class="text-center">
					Dr. Alejandro Valderrama Rodriguez <br>
					MÉDICO NEFROLOGO
				</th>
			</tr>
		</table>
	</footer>
</body>
</html>
