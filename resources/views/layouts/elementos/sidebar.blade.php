
<div class="sidebar sidebar-main sidebar-default sidebar-fixed">
    <div class="sidebar-content">
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="{{ asset('assets/images/image.png') }}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{ Auth::user()->usuario }}</span>
                        <div class="text-size-mini">
                            {{ collect(Auth::user()->roles)->first()->display_name }}
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="#user-nav" data-toggle="collapse"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li><a href="#" class="legitRipple"><i class="icon-cog5"></i> <span>Datos personales</span></a></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> <span>Cerrar sesión</span></a></li>
                </ul>
            </div>
        </div>


        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion navigation-icons-right navigation-xs">

                    <li><a href="{{ route('index') }}"><i class="icon-home4"></i> <span>Panel Administrativo</span></a></li>
                    
                    <li class="navigation-header"><span>Ficha Médica</span> <i class="icon-menu" title="Maestros"></i></li>
                    
                    <li><a href="{{ route('informe.index') }}"><i class="icon-profile"></i> <span>Informe médico</span></a></li>
                    <li><a href="{{ route('nefrologia.index') }}"><i class="icon-profile"></i> <span>Informe nefrología</span></a></li>
                    <li><a href="{{ route('evolucion.index') }}"><i class="icon-profile"></i> <span>Evolución médica</span></a></li>

                    <li class="navigation-header"><span>Maestros</span> <i class="icon-menu" title="Maestros"></i></li>

                    @permission(['agenda_create', 'agenda_edit', 'agenda_delete', 'agenda_show'])
                    <li><a href="{{ route('agenda.index') }}"><i class="icon-list-unordered"></i> <span>Agenda Médica</span></a></li>
                    @endpermission
                    
                    @permission(['paciente_create', 'paciente_edit', 'paciente_delete', 'paciente_show'])
                    <li><a href="{{ route('paciente.index') }}"><i class="icon-users"></i> <span>Pacientes</span></a></li>
                    @endpermission
                    
                    @permission(['medico_create', 'medico_edit', 'medico_delete', 'medico_show'])
                    @if (Auth::user()->hasRole(['med']))
                    <li><a href="{{ route('medico.edit', Auth::user()->medico->id) }}"><i class="icon-user-tie"></i> <span>Información de médico</span></a></li>
                    @else
                    <li><a href="{{ route('medico.index') }}"><i class="icon-user-tie"></i> <span>Médicos</span></a></li>
                    @endif
                    @endpermission
                </ul>
            </div>
        </div>

    </div>
</div>