<div class="navbar navbar-inverse bg-blue-600 navbar-fixed-top">
    <div class="navbar-header bg-blue-700">
        <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('assets/images/logo_light.png') }}" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            <li><a class="sidebar-mobile-opposite-toggle"><i class="icon-menu"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav no-margin no-padding">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

        </ul>

        <ul class="nav navbar-nav navbar-right">

            @ability('admin,medic', 'agenda_show')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle  bg-blue-700" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon-calendar3"></i>
                    <span class="visible-xs-inline-block position-right">Agenda del día</span>
                    <span class="badge bg-warning-400">1</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right  width-350">
                    <div class="dropdown-content-heading">
                        Agenda del día
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-eye"></i></a></li>
                        </ul>
                    </div>
                    <li><a href="#">Nombre de paciente <span class="label label-default pull-right">07:25</span></a></li>
                    
                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="" data-original-title="Ver agenda completa"><i class="icon-menu display-block"></i></a>
                    </div>
                </ul>
            </li>
            @endability
            
            @permission(['usuarios_create', 'usuarios_edit', 'usuarios_delete', 'usuarios_show'])
            <li>
                <a class="sidebar-control sidebar-opposite-fix hidden-xs">
                    <i class="icon-equalizer"></i>
                </a>
            </li>
            @endpermission
        </ul>
    
        {!! Form::open(['route' => 'usuario.configuracion.sucursal', 'class' => 'navbar-form navbar-right']) !!}
            <div class="input-group">
                {!! Form::select('sucursal_id', cboSucursales(), (Auth::user()->configuracion->sucursal_id) ?? null, ['class' => 'form-control selectpicker', 'placeholder' => 'Seleccione una sucursal para trabajar']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="icon-reset"></i></button>
                </span>
            </div>
        {!! Form::close() !!}
    </div>
</div>