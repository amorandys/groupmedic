<div class="page-header page-header-inverse has-cover no-padding">
	<div class="page-header-content">
		<div class="page-title">
			<h4>@yield('page-title')</h4>
			{!! Breadcrumbs::renderIfExists() !!}
		</div>
		<div class="heading-elements">
			
		</div>
	</div>
	<div class="navbar navbar-default navbar-xs "  >
        <ul class="nav navbar-nav visible-xs-block">
            <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-filter">
            <div class="navbar-right">
				@yield('opciones')
            </div>
        </div>
    </div>
</div>

