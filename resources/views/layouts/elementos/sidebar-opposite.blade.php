<div class="sidebar sidebar-opposite sidebar-inverse sidebar-fixed">

    <div class="sidebar-content">

        <div class="sidebar-category">
            <div class="category-title">
                <span><i class="icon-users position-left"></i>Usuarios</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="{{ route('usuario.index') }}" class="btn bg-teal-400 btn-block btn-float btn-float-lg">
                            <i class="icon-cogs"></i> <span>Administrar</span>
                        </a>
                    </div>
                    <div class="col-xs-6">
                        <a href="{{ route('roles.index') }}" class="btn bg-purple-300 btn-block btn-float btn-float-lg">
                            <i class="icon-equalizer2"></i> <span>Roles</span>
                        </a>
                    </div>


                </div>
            </div>
        </div>

        <div class="sidebar-category category-collapsed">
            <div class="category-title">
                <span><i class="icon-office position-left"></i>Sucursales</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-alt navigation-accordion">
                    <li><a href="{{ route('sucursal.index') }}"><i class="icon-menu6"></i> <span>Administrar</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>