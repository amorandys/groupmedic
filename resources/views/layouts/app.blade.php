<!DOCTYPE html>
{{--
App Name: GroupMedic
Version: 0.0.1
Author: Rosmar Blasquez
Website: 
Contact: reblasquez@gmail.com
Follow: 
Dribbble: 
Like: 
Purchase: 
Renew Support: 
License: 
--}}
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GroupMedic</title>

    {{-- Global stylesheets --}}
    {!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') !!}
    {!! Html::style('assets/css/icons/icomoon/styles.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/css/core.css') !!}
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/colors.css') !!}
    {!! Html::style('assets/css/extras/animate.min.css') !!}
    {!! Html::style('css/app.css') !!}
    {!! Html::style('css/sweetalert.css') !!}

    @stack('css')
    {{-- /global stylesheets --}}

    {{-- Core JS files --}}
    {!! Html::script('js/sweetalert.min.js') !!}
    {!! Html::script('assets/js/plugins/loaders/pace.min.js') !!}
    {!! Html::script('assets/js/core/libraries/jquery.min.js') !!}
    {!! Html::script('assets/js/core/libraries/bootstrap.min.js') !!}
    {!! Html::script('assets/js/plugins/ui/nicescroll.min.js') !!}
    {!! Html::script('assets/js/plugins/loaders/blockui.min.js') !!}
    {{-- /core JS files --}}

    {{-- Theme JS files --}}
    {!! Html::script('assets/js/plugins/forms/selects/bootstrap_select.min.js') !!}
    @stack('js-files')
    
    {!! Html::script('assets/js/core/app.js') !!}
    {!! Html::script('js/app.js') !!}
    {!! Html::script('assets/js/pages/layout_fixed_custom.js') !!}
    
    @stack('js')
    {{--  {!! Html::script('assets/js/plugins/ui/ripple.min.js') !!} --}}

    {{-- /theme JS files --}}
</head>

<body class="navbar-top">

    {{-- Navbar --}}
    @include('layouts.elementos.navbar')
    {{-- /Navbar --}}

    {{-- Page container --}}
    <div class="page-container">

        {{-- Page content --}}
        <div class="page-content">

            {{-- Main sidebar --}}
            @include('layouts.elementos.sidebar')
            {{-- /Main sidebar --}}

            {{-- Main content --}}
            <div class="content-wrapper">

                {{-- Page header --}}
                @include('layouts.elementos.page-header')
                {{-- /Page header --}}

                {{-- Content area --}}
                <div class="content">

                    @yield('content')
                    
                    {{-- sidebar-oposite --}}
                    @permission(['usuarios_create', 'usuarios_edit', 'usuarios_delete', 'usuarios_show'])
                    @include('layouts.elementos.sidebar-opposite')
                    @endrole
                    {{-- /sidebar-oposite --}}

                    {{-- footer --}}
                    @include('layouts.elementos.footer')
                    {{-- /footer --}}

                </div>
                {{-- /Content area --}}

            </div>
            {{-- /Main content --}}

        </div>
        {{-- /Page content --}}

    </div>
    {{-- /Page container --}}
    

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <a href="{{ route('digito.verificador', ':RUT') }}" class="hide digito-verificador"></a>
    @include('sweet::alert')
    
</body>
</html>
