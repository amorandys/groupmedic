@extends('layouts.app')

@section('page-title', 'Administrar roles')

@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel border-top-primary border-top-lg">
			<div class="panel-body">
				<ul class="media-list">
					@foreach ($roles as $role)
					<li class="media">
						<div class="media-left">
							<h5 class="no-margin text-left text-success">
								{{ str_pad($role->usuarios, 2, 0, STR_PAD_LEFT) }}
								<small class="display-block text-size-small no-margin">usuarios</small>
							</h5>
						</div>

						<div class="media-body">
							<span class="text-semibold">{{ $role->display_name }}</span>
							<ul class="list-inline list-inline-separate no-margin-bottom mt-5">
								<li>
									<span class="text-muted">{{ $role->description }} </span>
								</li>

							</ul>
						</div>
						<div class="media-right media-middle">

							<a class="btn btn-default btn-icon btn-rounded" href="{{ route('roles.edit', $role->id) }}">
								<i class="icon-equalizer2"></i>
							</a>

						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection