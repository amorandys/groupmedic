
@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
@endpush

@section('page-title', 'Configurar permisos del rol "'.$role->display_name.'"')

@push('css')
	<style>
		.pre-scrollable
		{
			height: 138px;
		}
	</style>
@endpush

@push('js')
<script>
	$(document).ready(function() {
		var arreglo = '<?php echo json_encode($role->perms); ?>'
		var l_a_json = eval('(' + arreglo + ')');
		$.each(l_a_json, function(index, val) {
			$('#'+val.name).prop('checked', true)
		});
	});
</script>
{!! Html::script('js/roles-edit.js') !!}
@endpush

@section('opciones')
<ul class="nav navbar-nav">
	<li>
		<a href="#" onclick="event.preventDefault(); document.getElementById('frm-actualizar-permisos').submit();">
			<i class="icon-database-refresh position-left"></i> Actualizar permisos
		</a>
	</li>

</ul>
@endsection

@section('content')
{!! Form::open(['route' => ['roles.update', $role->id], 'method' => 'PUT', 'id' => 'frm-actualizar-permisos']) !!}
<div class="row">
@foreach (collect($permisos)->groupBy('grupo') as $key => $value)
<div class="col-md-4">
	<div class="panel panel-flat no-border-radius border-top">
		<div class="panel-heading">
			<h5 class="panel-title text-capitalize text-semibold">{{ $key }}</h5>
		</div>
		<div class="panel-body">
			
			<div class="pre-scrollable">
				@foreach ($value as $permiso)


				<div class="checkbox">
					<label>
						{!! Form::checkbox('perm[]', $permiso->id, null, ['class' => 'styled', 'id' => $permiso->name]) !!}
						<em>{{ $permiso->display_name }} </em>
					</label>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endforeach
</div>
{!! Form::close() !!}
<div class="col-md-6">
</div>


@endsection