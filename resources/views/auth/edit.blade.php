@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}

@endpush

@push('js')
<script>
	$(document).ready(function() {
		var arreglo = '<?php echo json_encode($usuario->sucursales); ?>'
		var l_a_json = eval('(' + arreglo + ')');
		$.each(l_a_json, function(index, val) {
			console.log('sucursal'+val.sucursal_id)
			$('#sucursal'+val.sucursal_id).prop('checked', true)
		});
	});
</script>
{!! Html::script('js/usuario-edit.js') !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\UsuarioRequest', '#frmUsuario'); !!}
@endpush

@section('page-title', 'Registrar nuevo usuario')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	
	{!! Form::model($usuario, ['route' => ['usuario.update', $usuario->id], 'method' => 'PUT', 'id' => 'frmUsuario']) !!}
	
	<div class="col-md-4 col-md-offset-4">
		
		<div class="panel panel-flat border-top-primary border-top-lg">
			
			<div class="panel-body">

				<div class="form-group has-feedback has-feedback-left">
					{!! Form::label('usuario', '', ['class' => 'sr-only']) !!}
					{!! Form::text('usuario', null, ['class' => 'form-control text-bold', 'placeholder' => 'Usuario']) !!}
					<div class="form-control-feedback">
						<i class="icon-user-plus text-muted"></i>
					</div>
				</div>

				

				<div class="form-group has-feedback has-feedback-left">
					{!! Form::label('login', '', ['class' => 'sr-only']) !!}
					{!! Form::text('login', null, ['class' => 'form-control text-bold', 'placeholder' => 'Usuario de acceso']) !!}
					<div class="form-control-feedback">
						<i class="icon-user-check text-muted"></i>
					</div>
				</div>


				<div class="form-group has-feedback has-feedback-left">
					{!! Form::label('email', '', ['class' => 'sr-only']) !!}
					{!! Form::email('email', null, ['class' => 'form-control text-bold', 'placeholder' => 'Correo electrónico']) !!}
					<div class="form-control-feedback">
						<i class="icon-mention text-muted"></i>
					</div>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					{!! Form::label('tipo', 'Tipo de usuario', ['class' => 'sr-only']) !!}
					{!! Form::select('tipo', $role, null, ['class' => 'form-control text-bold', 'placeholder' => 'Tipo de usuario']) !!}
					<div class="form-control-feedback">
						<i class="icon-iphone text-muted"></i>
					</div>
				</div>

				

				<div class="form-group">
					<label class="text-semibold">Sucursales de acceso</label>
					@foreach ($sucursales as $sucursal)
					<div class="checkbox checkbox-right">
						<label>
							{{ Form::checkbox('sucursal[]', $sucursal->id, null, ['class' => 'styled', 'id' => 'sucursal'.$sucursal->id]) }}
							{{ $sucursal->nombre }}
						</label>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-flat border-top-primary border-top-lg">
			<div class="panel-body text-right">
				<button type="submit" class="btn btn-flat border-teal text-teal btn-labeled btn-labeled-right ml-10"><b><i class="icon-pencil7"></i></b> Actualizar información</button>
			</div>
		</div>
	</div>
	
	{!! Form::close() !!}
</div>

<a href="{{ route('digito.verificador', ':RUT') }}" class="hide digito-verificador"></a>
<a href="{{ route('global.comunas', ':ID') }}" class="hidden region_id"></a>
@endsection