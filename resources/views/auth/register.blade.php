@extends('layouts.app')

@push('js-files')

{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}

{!! Html::script('assets/js/plugins/notifications/pnotify.min.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}

{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}
@endpush

@push('js')
{!! Html::script('js/usuario-register.js') !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\UsuarioRequest', '#frmUsuario'); !!}

@endpush

@section('page-title', 'Registrar nuevo usuario')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>

	{!! Form::open(['route' => 'usuario.store', 'id' => 'frmUsuario', 'autocomplete' => 'off']) !!}
	
	<div class="col-md-8 col-md-offset-2">
		
		<div class="panel panel-flat border-top-primary border-top-lg">
			
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset>
							<legend><i class="icon-cog3 position-left"></i> Información Básica</legend>
							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('tipo', 'Tipo de usuario', ['class' => 'sr-only']) !!}
								{!! Form::select('tipo', $role, null, ['class' => 'form-control text-bold', 'placeholder' => 'Tipo de usuario']) !!}
								<div class="form-control-feedback">
									<i class="icon-iphone text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left" id="group-usuario">
								{!! Form::label('usuario', '', ['class' => 'sr-only']) !!}
								{!! Form::text('usuario', null, ['class' => 'form-control text-bold', 'placeholder' => 'Usuario']) !!}
								<div class="form-control-feedback">
									<i class="icon-user-plus text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left hide" id="group-nombres">
								{!! Form::label('nombres', '', ['class' => 'sr-only']) !!}
								{!! Form::text('nombres', null, ['class' => 'form-control text-bold', 'disabled' => 'disabled', 'placeholder' => 'Nombres']) !!}
								<div class="form-control-feedback">
									<i class="icon-user-plus text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left hide" id="group-apellidos">
								{!! Form::label('apellidos', '', ['class' => 'sr-only']) !!}
								{!! Form::text('apellidos', null, ['class' => 'form-control text-bold', 'disabled' => 'disabled', 'placeholder' => 'Apellidos']) !!}
								<div class="form-control-feedback">
									<i class="icon-user-plus text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('login', '', ['class' => 'sr-only']) !!}
								{!! Form::text('login', null, ['class' => 'form-control text-bold', 'placeholder' => 'Usuario de acceso']) !!}
								<div class="form-control-feedback">
									<i class="icon-user-check text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('email', '', ['class' => 'sr-only']) !!}
								{!! Form::email('email', null, ['class' => 'form-control text-bold', 'placeholder' => 'Correo electrónico']) !!}
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<label class="text-semibold">Sucursales de acceso</label>
								@foreach ($sucursales as $sucursal)
								<div class="checkbox checkbox-right">
									<label>
										{{ Form::checkbox('sucursal[]', $sucursal->id, null, ['class' => 'styled', 'id' => 'sucursal'.$sucursal->id]) }}
										{{ $sucursal->nombre }}
									</label>
								</div>
								@endforeach
							</div>
						</fieldset>

					</div>
					<div class="col-md-6">
						<fieldset class="hide"  id="informacion_adicional">
							<legend><i class="icon-cog3 position-left"></i> Información complementaria</legend>
							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('rut', '', ['class' => 'sr-only']) !!}
								<div class="input-group">
									{!! Form::text('rut', null, ['class' => 'form-control text-bold', 'placeholder' => 'RUT']) !!}
									<span class="input-group-addon">-&nbsp;&nbsp; </span>
								</div>
								<div class="form-control-feedback">
									<i class="icon-vcard text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('fecha_nacimiento', '', ['class' => 'sr-only']) !!}
								{!! Form::text('fecha_nacimiento', null, ['class' => 'form-control pickadate text-bold', 'placeholder' => 'Fecha de nacimiento']) !!}
								<div class="form-control-feedback">
									<i class="icon-calendar2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								{!! Form::label('sexo', '', ['class' => 'sr-only']) !!}
								{!! Form::select('sexo', ['masculino' => 'Masculino', 'femenino' => 'Femenino'], null, ['class' => 'form-control text-bold', 'placeholder' => 'Indique el sexo']) !!}
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('telefono', '', ['class' => 'sr-only']) !!}
								{!! Form::text('telefono', null, ['class' => 'form-control text-bold', 'placeholder' => 'Teléfono']) !!}
								<div class="form-control-feedback">
									<i class="icon-phone text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('celular', '', ['class' => 'sr-only']) !!}
								{!! Form::text('celular', null, ['class' => 'form-control text-bold', 'placeholder' => 'Celular']) !!}
								<div class="form-control-feedback">
									<i class="icon-iphone text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('region_id', '', ['class' => 'sr-only']) !!}
								{!! Form::select('region_id', $regiones,null, ['class' => 'form-control text-bold', 'rows' => 3, 'placeholder' => 'Region']) !!}
								<div class="form-control-feedback">
									<i class="icon-city text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('comuna_id', '', ['class' => 'sr-only']) !!}
								{!! Form::select('comuna_id', [],null, ['class' => 'form-control text-bold', 'rows' => 3, 'placeholder' => 'Comuna']) !!}
								<div class="form-control-feedback">
									<i class="icon-office text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::label('direccion', '', ['class' => 'sr-only']) !!}
								{!! Form::textArea('direccion', null, ['class' => 'form-control text-bold', 'rows' => 3, 'placeholder' => 'Dirección']) !!}
								<div class="form-control-feedback">
									<i class="icon-home2 text-muted"></i>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
	
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-flat border-top-primary border-top-lg">
			<div class="panel-body text-right">
				<button type="submit" class="btn btn-flat border-teal text-teal btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Registrar usuario</button>
			</div>
		</div>
	</div>
	
	{!! Form::close() !!}
</div>

<a href="{{ route('digito.verificador', ':RUT') }}" class="hide digito-verificador"></a>
<a href="{{ route('global.comunas', ':ID') }}" class="hidden region_id"></a>
@endsection