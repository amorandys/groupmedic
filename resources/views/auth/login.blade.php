<!DOCTYPE html>
<!--
App Name: GroupMedic
Version: 0.0.1
Company: SC Informática
Author: Rosmar Blasquez
Website: 
Contact: desarrollo@scinformatica.cl
		 reblasquez@gmail.com
Follow: 
Like: 
Purchase: 
Renew Support: 
License: 
-->
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GroupMedic</title>

    {{-- Global stylesheets --}}
    {!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') !!}
    {!! Html::style('assets/css/icons/icomoon/styles.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/css/core.css') !!}
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/colors.css') !!}

    @stack('css')
    {{-- /global stylesheets --}}

    {{-- Core JS files --}}
    {!! Html::script('assets/js/plugins/loaders/pace.min.js') !!}
    {!! Html::script('assets/js/core/libraries/jquery.min.js') !!}
    {!! Html::script('assets/js/core/libraries/bootstrap.min.js') !!}
    {!! Html::script('assets/js/plugins/ui/nicescroll.min.js') !!}
    {!! Html::script('assets/js/plugins/loaders/blockui.min.js') !!}
    {{-- /core JS files --}}

    {{-- Theme JS files --}}
    @stack('js-files')
    
    {!! Html::script('assets/js/core/app.js') !!}
    {!! Html::script('assets/js/pages/layout_fixed_custom.js') !!}
    
    @stack('js')
    {{-- /theme JS files --}}
</head>

<body class="login-container login-cover">

	<div class="navbar navbar-inverse bg-blue-800">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>
	</div>

	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
					{!! Form::open(['route' => 'login', 'autocomplete' => 'off']) !!}
						<div class="panel panel-body no-border-radius login-form">
							<div class="text-center">
								<div class="icon-object no-border"><img src="assets/images/image.png" style="max-height: 80px" class="img-circle img-responsive" alt=""></div>

								<h5 class="content-group">Ingrese a su cuenta <small class="display-block">Ingrese sus credenciales a continuación</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::text('login', null, ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña']) !!}
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn border-slate text-slate-800 btn-flat legitRipple no-border-radius btn-block">Ingresar <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="text-center">
								<a href="login_password_recover.html">¿Olvidaste tu contraseña?</a>
							</div>
						</div>
					{!! Form::close() !!}
			@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
				</div>
			</div>

		</div>
	</div>
</body>
</html>