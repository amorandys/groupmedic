@extends('layouts.app')

@section('page-title', 'Maestro de Usuarios')

@section('opciones')
<ul class="nav navbar-nav">
	<li>
		<a href="{{ route('usuario.create') }}" >
			<i class="icon-user-plus position-left"></i> Ingresar usuario
		</a>
	</li>

</ul>
@endsection

@section('content')

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		
		<div class="panel panel-flat border-top-primary border-top-lg">
			<div class="panel-body">
				<ul class="media-list">
					@foreach ($usuarios as $usuario)
					<li class="media">
						<div class="media-body">
							<div class="media-heading text-semibold">{{ $usuario->usuario }}</div>
							<span class="text-muted">
							@foreach ($usuario->roles as $element)
								{{ $element->display_name }}
							@endforeach
							</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-extended text-nowrap">
								<li>
									<a class="text-muted" href="#" data-popup="tooltip" title="" data-original-title="ver/auditar"><i class="icon-eye"></i></a>
								</li>
								<li>
									<a class="text-muted" href="{{ route('usuario.edit', $usuario->id) }}" data-popup="tooltip" title="" data-original-title="editar"><i class="icon-pencil6"></i></a>
								</li>
								<li>
									<a class="text-warning" href="#" data-popup="tooltip" title="" data-original-title="eliminar"><i class="icon-trash"></i></a>
								</li>
							</ul>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="panel-footer text-right border-primary p-15">
				{{ $usuarios->links() }}
			</div>
		</div>
	</div>
</div>
@endsection