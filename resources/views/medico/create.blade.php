@extends('layouts.app')

@section('page-title', 'Maestro de Médicos')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('assets/js/plugins/forms/selects/select2.min.js') !!}

{!! Html::script('assets/js/plugins/notifications/pnotify.min.js') !!}
{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}
@endpush

@push('js')
{!! Html::script('js/medico-create.js') !!}
@endpush


@section('content')
{!! Form::open(['route' => 'medico.store', 'autocomplete' => 'off']) !!}
<div class="row">
	<div class="col-md-8 col-lg-offset-2">
		<div class="panel panel-flat border-top-lg border-top-primary">
			<div class="panel-heading">
				<h5 class="panel-title">Datos personales</h5>
			</div>
			<div class="panel-body">
				
				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('rut', 'RUT', ['class' => '']) !!}
						{!! Form::text('rut', null, ['class' => 'form-control input-roundless text-bold',  'data-mask' => '99.999.999-9']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('nombres', '', ['class' => '']) !!}
						{!! Form::text('nombres', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('apellidos', 'Apellidos', ['class' => '']) !!}
						{!! Form::text('apellidos', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('sexo', '', ['class' => '']) !!}
						{!! Form::select('sexo', ['masculino' => 'Masculino', 'femenino' => 'Femenino'], null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento', ['class' => '']) !!}
						{!! Form::date('fecha_nacimiento', null, ['class' => 'form-control input-roundless text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('edo_civil', 'Estado Civil', ['class' => '']) !!}
						{!! Form::select('pais_id', [], null, ['class' => 'form-control input-roundless text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('pais_id', 'País de nacimiento', ['class' => '']) !!}
						{!! Form::select('pais_id', [], null, ['class' => 'form-control input-roundless text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('pais_id', 'Nacionalidad', ['class' => '']) !!}
						{!! Form::select('pais_id', [], null, ['class' => 'form-control input-roundless text-bold', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>
		</div>
		
		<div class="panel panel-flat border-top-lg border-top-primary">
			<div class="panel-heading">
				<h5 class="panel-title">Información de contacto</h5>
			</div>
			<div class="panel-body">
				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('telefono', '', ['class' => '']) !!}
						{!! Form::text('telefono', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('celular', '', ['class' => '']) !!}
						{!! Form::text('celular', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group form-group-xs">
						{!! Form::label('email', '', ['class' => '']) !!}
						{!! Form::text('email', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group form-group-xs">
						{!! Form::label('direccion', 'Dirección', ['class' => '']) !!}
						{!! Form::text('direccion', null, ['class' => 'form-control text-bold']) !!}
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-flat border-top-lg border-top-danger">
			<div class="panel-body text-right">
				<button type="submit" class="btn border-slate text-slate-800 btn-flat legitRipple"><i class="icon-file-plus position-left"></i> Guardar información</button>
			</div>
		</div>
	</div>
</div>


@endsection