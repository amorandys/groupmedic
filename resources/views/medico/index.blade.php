@extends('layouts.app')

@section('page-title', 'Maestro de Médicos')

@push('js-files')
{!! Html::script('assets/js/plugins/tables/datatables/datatables.min.js') !!}
@endpush

@push('js')
{!! Html::script('https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js') !!}
{!! Html::script('/vendor/datatables/buttons.server-side.js') !!}
@endpush

@section('opciones')
<ul class="nav navbar-nav">
	<li>
		<a href="{{ route('medico.create') }}" >
			<i class="icon-user-plus position-left"></i> Agregar médico
		</a>
	</li>

</ul>
@endsection

@section('content')
<div class="panel panel-flat">
	<div class="panel-body">
		{!! $dataTable->table() !!}
	</div>
</div>

{!! $dataTable->scripts() !!}
@endsection