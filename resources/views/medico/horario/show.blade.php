@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.time.js') !!}
@endpush

@push('js')
	{!! Html::script('js/horario-show.js') !!}
@endpush

@section('page-title', 'Horario de médico')

@section('content')
<div class="row">
	{!! Form::open(['route' => ['horario.update', $medico->id], 'method' => 'PUT']) !!}
	<div class="col-md-4">
		<div class="panel border-top-primary">
			<div class="panel-body">

				<div class="form-group">
					{!! Form::label('dia', 'Dia de atencion', ['class' => 'sr-only']) !!}
					{!! Form::select('dia', ['lunes' => 'Lunes', 'martes' => 'Martes', 'miercoles' => 'Mercoles', 'jueves' => 'Jueves', 'viernes' => 'Viernes', 'sabado' => 'Sabado', 'domingo' => 'Domingo'], null, ['class' => 'form-control selectpicker', 'placeholder' => 'Día de atención']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('hora_ini', 'Inicio de atenciones', ['class' => 'sr-only']) !!}
					{!! Form::text('hora_ini', null, ['class' => 'form-control pickatime input-xs', 'placeholder' => 'Inicio de atenciones']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('hora_fin', 'Fin de atenciones', ['class' => 'sr-only']) !!}
					{!! Form::text('hora_fin', null, ['class' => 'form-control pickatime input-xs', 'placeholder' => 'Fin de atenciones']) !!}
				</div>

			</div>
			<div class="panel-footer text-right pr-20">
				<button type="submit" class="btn btn-flat text-primary border-primary">Guardar</button>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
	<div class="col-md-8">
		<div class="panel border-top-primary">
			<div class="panel-body no-padding">
				<table class="table">
					<thead>
						<tr>
							<th>Lunes</th>
							<th>Martes</th>
							<th>Miercoles</th>
							<th>Jueves</th>
							<th>Viernes</th>
							<th>Sabado</th>
							<th>Domingo</th>
						</tr>
					</thead>
					<tbody>
					@for ($i = 0; $i < 3; $i++)
						<tr>
							@for ($j = 0; $j < 7; $j++)
							<td>&nbsp;</td>
							@endfor
						</tr>
					@endfor
					</tbody>
				</table>
			</div>
		</div>

		<div class="panel border-top-primary">
			<div class="panel-body">
				@foreach ($medico->horario as $element)
					<p>{{ $element->dia }}</p>
					<p>{{ $element->turno }}</p>
					<p>{{ $element->hora_ini }}</p>
					<p>{{ $element->hora_fin }}</p>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection