@extends('layouts.impresion')

@section('content')
<table class="table no-border">
	<tbody>
		<tr>
			<td>Paciente</td>
			<td>{{ $informe->paciente->nombres }}, {{ $informe->paciente->apellidos }}</td>
			<td>Fecha de informe</td>
			<td>{{ $informe->fecha_informe }}</td>
		</tr>
		<tr>
			<td>Edad</td>
			<td>
				@php
				use Carbon\Carbon;

				list($y,$m,$d) = explode("-", $informe->paciente->fecha_nacimiento);
				$edad = Carbon::createFromDate($y, $m, $d)->age;
				echo $edad.' años';
				@endphp
			</td>
			<td>RUT</td>
			<td>{{ number_format($informe->paciente->rut, 0, '', '.').'-'.$informe->paciente->rut_v }}</td>
		</tr>
	</tbody>
</table>

<table class="table">
	<tbody>
		<tr>
			<th colspan="3">DIAGNOSTICO</th>
		</tr>
		<tr>
			<td colspan="3">{!! $informe->diagnostico !!}</td>
		</tr>
		<tr>
			<th colspan="3">TRATAMIENTO</th>
		</tr>
		<tr>
			<td colspan="3">{!! $informe->tratamiento !!}</td>
		</tr>
		<tr>
			<th colspan="3">OBSERVACIONES</th>
		</tr>
		<tr>
			<td colspan="3">{!! $informe->observaciones !!}</td>
		</tr>
	</tbody>
</table>
@endsection