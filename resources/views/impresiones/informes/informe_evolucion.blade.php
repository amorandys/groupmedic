@extends('layouts.impresion')

@section('informe-titulo', 'Evolución médica mensual')

@section('content')
<table class="table no-border">
	<tbody>
		<tr>
			<td>Paciente</td>
			<td>{{ $informe->paciente->nombres }}, {{ $informe->paciente->apellidos }}</td>
			<td>Fecha de informe</td>
			<td>{{ $informe->fecha_informe }}</td>
		</tr>
		<tr>
			<td>Edad</td>
			<td>
				@php
				use Carbon\Carbon;

				list($y,$m,$d) = explode("-", $informe->paciente->fecha_nacimiento);
				$edad = Carbon::createFromDate($y, $m, $d)->age;
				echo $edad.' años';
				@endphp
			</td>
			<td>RUT</td>
			<td>{{ number_format($informe->paciente->rut, 0, '', '.').'-'.$informe->paciente->rut_v }}</td>
		</tr>
	</tbody>
</table>

<table class="table padding-mid table-condensed">
	<tbody>
		<tr>
			<td width="25%">Estado General</td>
			<td width="25%">{{ $informe->estado }}</td>
			<td width="25%">Tolerancia a la HD</td>
			<td width="25%">{{ $informe->tolerancia_hd }}</td>
		</tr>
		<tr>
			<td>Complicaciones Intradialisis</td>
			<td>{{ $informe->compli_intra }}</td>
			<td>Llega al peso seco</td>
			<td>{{ $informe->peso_seco }}</td>
		</tr>
		<tr>
			<td>Causa</td>
			<td colspan="3">{{ $informe->causa_compli_intra }}</td>
		</tr>
		<tr><th colspan="4" class="text-center">Complicaciones Cardiovasculares</th></tr>
		<tr>
			<td>HTA controlada</td>
			<td>{{ ($informe->compli_cardio) ? 'Si' : 'No' }}</td>
			<td>Arritmias</td>
			<td>{{ ($informe->compli_cardio) ? 'Si' : 'No' }}</td>
		</tr>
		<tr>
			<td>Causa</td>
			<td colspan="3">{{ $informe->arritmia_causa }}</td>
		</tr>
		<tr>
			<td>Estado hematológico (Causa/Alteración)</td>
			<td colspan="3">{{ $informe->edo_osteometabolico }}</td>
		</tr>
		<tr>
			<td>Usa EPO (Dosis/Semanal)</td>
			<td >{{ $informe->usa_epo_dosis }}</td>
			<td>Usa hierro IV (Dosis)</td>
			<td>{{ $informe->hierro }}</td>
		</tr>
		<tr>
			<td>Trasnfusión (Cantidad)</td>
			<td colspan="3">{{ $informe->transfusion_cantidad }}</td>
		</tr>
		<tr>
			<td>Estado osteometabólico / Acido base</td>
			<td colspan="3">{{ $informe->edo_osteometabolico }}</td>
		</tr>
		<tr>
			<td>Usa derivados Vit-D (Tipo/Dosis)</td>
			<td colspan="3">{{ $informe->deriv_vit_d }}</td>
		</tr>
		<tr>
			<td>Depuración de urea</td>
			<td colspan="3">{{ $informe->dep_urea }}</td>
		</tr>
		<tr>
			<td>Estado nutricional</td>
			<td colspan="3">{{ $informe->edo_nutricional }}</td>
		</tr>
		<tr>
			<td>Acceso vascular (Tipo/función)</td>
			<td colspan="3">{{ $informe->acc_vascular }}</td>
		</tr>
		<tr>
			<td>Plan terapeutico</td>
			<td colspan="3">{{ $informe->plan_terapeutico }}</td>
		</tr>
		<tr>
			<td>Urgencia/Hospitalización</td>
			<td>{{ $informe->urgencia_hospitalizacion }}</td>
			<td>Resuelto</td>
			<td>{{ $informe->resuelto }}</td>
		</tr>
		<tr>
			<td>Motivo</td>
			<td colspan="3">{{ $informe->motivo_consulta }}</td>
		</tr>
		<tr>
			<td>Evaluación médica quirurgica pendiente</td>
			<td colspan="3">{{ $informe->evaluacion }}</td>
		</tr>
		<tr>
			<td>Solicitud RX torax</td>
			<td>{{ $informe->solicitud_rx }}</td>
			<td>Programa TX renal (estado)</td>
			<td>{{ $informe->progragrama_tx }}</td>
		</tr>
	</tbody>
</table>

@endsection