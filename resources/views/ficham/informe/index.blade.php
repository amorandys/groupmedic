@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}


{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}


{!! Html::script('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') !!}
@endpush

@push('js')
{!! Html::script('js/ficham-informe-register.js') !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}

{!! JsValidator::formRequest('App\Http\Requests\InformeMedicoRequest', '#frmInforme'); !!}
@endpush

@section('page-title', 'Informe médico')

@section('opciones')
<ul class="nav navbar-nav">
	<li class="active"><a href="#" data-toggle="modal" data-target="#mdPaciente"><i class="icon-users2 position-left"></i> Agregar paciente</a></li>
</ul>
@endsection

@section('content')
<div class="alert alert-warning alert-styled-right hide" id="alert">
	<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
	<span class="text-semibold">Warning!</span> Better <a href="#" class="alert-link">check yourself</a>, you're not looking too good.
	<ul class="list-condensed error"></ul>
</div>
{!! Form::open(['route' => 'informe.store', 'id' => 'frmInforme']) !!}
<div class="panel panel-flat border-top-primary">

	<div class="panel-body">
		@include('ficham.elementos.infCabecera')

		<fieldset>
			<legend class="text-semibold">
				<i class="icon-reading position-left"></i>
				Detalles del Informe
			</legend>
				<div class="form-group">
					{!! Form::label('diagnostico', 'Diagnóstico', ['class' => '']) !!}
					{!! Form::textArea('diagnostico', null, ['class' => 'form-control', 'rows' => 5]) !!}
				</div>

				<div class="form-group">
					{!! Form::label('tratamiento', '', ['class' => '']) !!}
					{!! Form::textArea('tratamiento', null, ['class' => 'form-control', 'rows' => 5]) !!}
				</div>

				<div class="form-group">
					{!! Form::label('observaciones', '', ['class' => '']) !!}
					{!! Form::textArea('observaciones', null, ['class' => 'form-control', 'rows' => 5]) !!}
				</div>
		</fieldset>
		
	</div>
	<div class="panel-footer text-right pr-20">
		<button type="submit" class="btn btn-primary">Cargar informe <i class="icon-arrow-right14 position-right"></i></button>
	</div>
</div>
{!! Form::close() !!}


@include('ficham.elementos.modalPacienteCreate')


@endsection