@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}
{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}


@endpush

@push('js')
{!! Html::script('js/ficham-evolucion-register.js') !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}

{!! JsValidator::formRequest('App\Http\Requests\EvolucionMedicaRequest', '#frmEvolucionMedica'); !!}
@endpush
{{-- tu diras porque todo ese poco de flechas pues porque las tablas estan relacionadas aca --}}
@section('page-title', $informe->paciente->nombres.' '.$informe->paciente->apellidos)

@section('opciones')
<ul class="nav navbar-nav">
	<li class="active"><a href="#" data-toggle="modal" data-target="#mdPaciente"><i class="icon-users2 position-left"></i> Editar Evolución del Paciente</a></li>
</ul>
@endsection

@section('content')
{{-- esta linea es importante porque es la que trae el arreglo del controlador al formulario es algo asi como traeme el informe TAL del paciente TAL Y LLAMA AL EVOLUCION.UPDATE  me equivoque es un controlador tambien
este update tiene al final una vaina que arma el boton y le muestra al usuario el aviso de grabado satisfactoriamente y ahi me esta dando un error muñeca jeje!!!
--}}
{!! Form::model($informe->toArray()+$informe->paciente->toArray(), ['route' => ['evolucion.update', $informe->id], 'method' => 'PUT', 'autocomplete' => 'off', 'id' => 'frmEvolucionMedica']) !!}

<div class="panel panel-flat border-top-primary">

	<div class="panel-body">

		
		{{--  @include('ficham.elementos.infCabecera')--}}
		
		<fieldset>
			<legend class="text-semibold">
				<table width='50%'>
					<tr>
						<h4><i class="icon-reading position-left"></i>
						Detalles del Informe Paciente: <strong>{{ $informe->paciente->nombres }} {{ $informe->paciente->apellidos }}</strong></h4>
					</tr>
					<tr>
						<h5>Folio: <strong>{{ $informe->id }}</strong> Fecha: <strong>{{ $informe->fecha_informe }}</strong></h5>
					</tr>
				</table>
				
				
			</legend>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group form-group-xs">
						{!! Form::label('estado', 'Estado general', ['class' => '']) !!}
						{!! Form::select('estado', ['adecuado' => 'Adecuado', 'estable' => 'Estable', 'malo' => 'Malo'], null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group form-group-xs">
						{!! Form::label('tolerancia_hd', 'Tolerancia a la HD', ['class' => '']) !!}
						{!! Form::select('tolerancia_hd', ['adecuado' => 'Adecuado', 'estable' => 'Estable', 'malo' => 'Malo'], null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>
		</fieldset>

		<hr>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group form-group-xs">
					{!! Form::label('compli_intra', 'Complicaciones Intradialisis', ['class' => 'control-label col-md-6']) !!}
					<div class="col-md-6">
						{!! Form::text('compli_intra', null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="form-group form-group-xs">
					{!! Form::label('peso_seco', 'Llega al peso seco', ['class' => 'control-label col-md-6']) !!}
					<div class="col-md-6">
						{!! Form::text('peso_seco', null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('causa_compli_intra', 'Causa', ['class' => 'control-label col-md-2']) !!}
					<div class="col-md-10">
						{!! Form::textArea('causa_compli_intra', null, ['class' => 'form-control', 'placeholder' => '', 'rows' => 3]) !!}
					</div>
				</div>
			</div>
		</div>


		<fieldset>
			<legend class="text-semibold">
				<i class="icon-heart-broken2 position-left"></i>
				Complicaciones Cardiovasculares

			</legend>
			<div class="row form-horizontal">
				<div class="col-md-6">
					<div class="form-group form-group-xs">
						{!! Form::label('compli_cardio', 'Complicaciones cardiovasculares', ['class' => 'control-label col-md-4']) !!}
						<div class="col-md-8">
							<label class="checkbox-inline">
								{!! Form::checkbox('compli_cardio', true, false, ['class' => 'styled']) !!} Si
							</label>
						</div>
					</div>

					<div class="form-group form-group-xs">
						{!! Form::label('arritmia', 'Arritmias', ['class' => 'control-label col-md-4']) !!}
						<div class="col-md-8">
							<label class="checkbox-inline">
								{!! Form::checkbox('arritmia', true, false, ['class' => 'styled']) !!} Si
							</label>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('arritmia_causa', '', ['class' => 'control-label col-md-2']) !!}
						<div class="col-md-10">
							{!! Form::textArea('arritmia_causa', null, ['class' => 'form-control', 'placeholder' => '', 'rows' => 3]) !!}
						</div>
					</div>
				</div>
			</div>
		</fieldset>
		
		<hr>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('edo_hermatologico', 'Estado Hematológico (Causa/Alteración)', ['class' => 'label-control col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('edo_hermatologico', null, ['class' => 'form-control', 'placeholder' => '', 'rows' => 4]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group form-group-xs">
					{!! Form::label('usa_epo_dosis', 'Usa EPO (Dosis/Semanal)', ['class' => 'label-control col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::text('usa_epo_dosis', null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="form-group form-group-xs">
					{!! Form::label('hierro', 'Usa hierro IV (Dosis/Semanal)', ['class' => 'label-control col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::text('hierro', null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>
		</div>
		
		<hr>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('transfusion_cantidad', 'Transfusiones (Cantidad)', ['class' => 'label-control col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::text('transfusion_cantidad', null, ['class' => 'form-control', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('edo_osteometabolico', 'Estado Osteometabolico / Acido-Base', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('edo_osteometabolico', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('deriv_vit_d', 'Usa derivados Vit-D (Tipo/Dosis)', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('deriv_vit_d', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('dep_urea', 'Depuración Urea', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('dep_urea', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('edo_nutricional', 'Estado nutricional', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('edo_nutricional', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('acc_vascular', 'Acceso vascular', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('acc_vascular', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('plan_terapeutico', '', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('plan_terapeutico', null, ['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>
			
		</div>

		<hr>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('urgencia_hospitalizacion', 'Consulta Urgencias/Hospitalización', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-6">
						<label class="checkbox-inline">
							{!! Form::checkbox('urgencia_hospitalizacion', true, false,['class' => 'styled']) !!} Si
						</label>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('resuelto', '', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-6">
						<label class="checkbox-inline">
							{!! Form::checkbox('resuelto', true, false, ['class' => 'styled']) !!} Si
						</label>
					</div>
				</div>
			</div>

		</div>
		
		<hr>
		<div class="row form-horizontal">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('motivo_consulta', 'Motivo', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('motivo_consulta', null,['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('evaluacion', 'Evaluación médica o quirúrgica pendiente', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::textArea('evaluacion', null,['class' => 'form-control', 'rows' => 3]) !!}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('solicitud_rx', 'Solicitud de RX torax', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						<label class="checkbox-inline">
							{!! Form::checkbox('solicitud_rx', true, false, ['class' => 'styled']) !!} Si
						</label>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('progragrama_tx', 'Programa TX renal (Etado)', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
							{!! Form::text('progragrama_tx', null, ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel-footer text-right pr-20">
			<button type="submit" class="btn btn-primary">Actualizar Informe <i class="icon-arrow-right14 position-right"></i></button>
		</div>
	</div>
</div>
{!! Form::close() !!}

@include('ficham.elementos.modalPacienteCreate')

@endsection