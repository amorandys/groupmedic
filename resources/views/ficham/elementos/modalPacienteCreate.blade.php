<a href="{{ route('paciente.comprobar', ':ID') }}" class="comprobar"></a>

<div class="modal fade" id="mdPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Paciente <small class="display-block">Agregar información básica</small></h4>
			</div>
			{!! Form::open(['route' => 'paciente.store', 'class' => 'form-horizontal']) !!}
			<div class="modal-body">

				<div class="form-group  form-group-xs">
					{!! Form::label('rut_register', 'RUT', ['class' => 'label-control col-md-4']) !!}
					<div class="col-md-8">
						<div class="input-group">
							{!! Form::text('rut_register', null, ['class' => 'form-control text-bold rut']) !!}
							<span class="input-group-addon">-&nbsp;&nbsp; </span>
						</div>
					</div>
				</div>


				<div class="form-group form-group-xs">
					{!! Form::label('nombres', '', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::text('nombres', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>

				<div class="form-group form-group-xs">
					{!! Form::label('apellidos', 'Apellidos', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::text('apellidos', null, ['class' => 'form-control input-roundless text-bold']) !!}
					</div>
				</div>
				
				
				<div class="form-group form-group-xs">
					{!! Form::label('sexo', '', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::select('sexo', ['masculino' => 'Masculino', 'femenino' => 'Femenino'], null, ['class' => 'form-control text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="form-group form-group-xs">
					{!! Form::label('edo_civil', 'Estado Civil', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::select('edo_civil', ['soltero' => 'Soltero', 'casado' => 'Casado'], null, ['class' => 'form-control text-bold', 'placeholder' => '']) !!}
					</div>
				</div>
				
				<div class="form-group form-group-xs">
					{!! Form::label('pais_id', 'País de nacimiento', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::select('pais_nacimiento_id', [], null, ['class' => 'form-control select2 text-bold', 'placeholder' => '']) !!}
					</div>
				</div>

				<div class="form-group form-group-xs">
					{!! Form::label('pais_id', 'Nacionalidad', ['class' => 'control-label col-md-4']) !!}
					<div class="col-md-8">
						{!! Form::select('pais_nacionalidad_id', [], null, ['class' => 'form-control select2 text-bold', 'placeholder' => '']) !!}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				@submit_button(Guardar información)
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>