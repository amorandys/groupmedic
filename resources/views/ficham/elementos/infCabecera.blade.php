<fieldset>
	<legend class="text-semibold">
		<i class="icon-file-text2 position-left"></i>
		Cabecera del informe
	</legend>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group form-group-sm">
				{!! Form::label('rut_paciente', 'RUT Paciente', ['class' => '']) !!}
				<div class="input-group">
					{!! Form::text('rut_paciente', null, ['class' => 'form-control rut']) !!}
					<span class="input-group-addon">-&nbsp;&nbsp; </span>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group form-group-sm">
				{!! Form::label('paciente_info', '&nbsp;', ['class' => '']) !!}
				{!! Form::text('paciente_info', null, ['class' => 'form-control rut', 'disabled' => 'disabled']) !!}
			</div>
			<span class="paciente-nombre"></span>
		</div>

		<div class="col-md-3">
			<div class="form-group form-group-sm">
				{!! Form::label('sucursal_id', 'Sucursal', ['class' => '']) !!}
				{!! Form::text('sucursal_id', Auth::user()->configuracion->sucursal->nombre, ['class' => 'form-control rut', 'disabled' => 'disabled']) !!}
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group form-group-sm">
				{!! Form::label('fecha_informe', '', ['class' => '']) !!}
				{!! Form::text('fecha_informe', date('d/m/Y'), ['class' => 'form-control pickadate']) !!}
			</div>
		</div>
	</div>
</fieldset>