@extends('layouts.app')

@push('js-files')
{!! Html::script('assets/js/plugins/forms/styling/uniform.min.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.js') !!}
{!! Html::script('assets/js/plugins/pickers/pickadate/picker.date.js') !!}
{!! Html::script('assets/js/plugins/forms/inputs/formatter.min.js') !!}

@endpush

@push('js')
{!! Html::script('js/ficham-nefrologia-register.js') !!}
{!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}

{!! JsValidator::formRequest('App\Http\Requests\InformeNefrologicoRequest', '#frmInformeNefrologico'); !!}
@endpush

@section('page-title', 'Nefrología')

@section('opciones')
<ul class="nav navbar-nav">
	<li class="active"><a href="#" data-toggle="modal" data-target="#mdPaciente"><i class="icon-users2 position-left"></i> Agregar paciente</a></li>
</ul>
@endsection

@section('content')
{!! Form::model($informe->toArray()+$informe->paciente->toArray(), ['route' => ['nefrologia.update', $informe->id], 'method' => 'PUT', 'autocomplete' => 'off', 'id' => 'frmEvolucionMedica']) !!}

{!! Form::open(['route' => 'nefrologia.store', 'id' => 'frmInformeNefrologico']) !!}
<div class="panel panel-flat border-top-primary">

	<div class="panel-body">
		{{-- @include('ficham.elementos.infCabecera') --}}
		
		<fieldset>
			<legend class="text-semibold">
				<table width='50%'>
					<tr>
						<h4><i class="icon-reading position-left"></i>
						Detalles del Informe Paciente: <strong>{{ $informe->paciente->nombres }} {{ $informe->paciente->apellidos }}</strong></h4>
					</tr>
					<tr>
						<h5>Folio: <strong>{{ $informe->id }}</strong> Fecha: <strong>{{ $informe->fecha_informe }}</strong></h5>
					</tr>
				</table>
				{{--  <i class="icon-reading position-left"></i>
				Detalles del Informe
				--}}
			</legend>

			<div class="form-group">
				{!! Form::label('diagnostico', 'Diagnóstico', ['class' => '']) !!}
				{!! Form::textArea('diagnostico', null, ['class' => 'form-control', 'rows' => 3]) !!}
			</div>
		</fieldset>
		
		<div class="row">
			<div class="col-md-6">
				

				<fieldset>
					<legend class="text-semibold text-center">
						<i class="icon-copy position-left"></i>
						Parametros de diálisis
						
					</legend>


					<div class="row form-horizontal">

						<div class="col-md-6">
							<div class="form-group form-group-xs">
								{!! Form::label('d_peso', 'Peso Seco', ['class' => 'col-lg-5 control-label']) !!}
								<div class="col-lg-7">
									{!! Form::text('d_peso', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_qb', 'QB', ['class' => 'col-lg-5 control-label']) !!}
								<div class="col-lg-7">
									{!! Form::text('d_qb', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_qtipo', 'QT', ['class' => 'col-lg-5 control-label']) !!}
								<div class="col-lg-7">
									{!! Form::text('d_qtipo', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_flujo', 'Flujo', ['class' => 'col-lg-5 control-label']) !!}
								<div class="col-lg-7">
									{!! Form::text('d_flujo', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_filtro', 'Filtro', ['class' => 'col-lg-5 control-label']) !!}
								<div class="col-lg-7">
									{!! Form::text('d_filtro', null, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group form-group-xs">
								{!! Form::label('d_heparina', 'Heparina Ini/Man', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('d_heparina', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_tipo_acceso_vascular', 'Acceso Vascular', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('d_tipo_acceso_vascular', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_k', 'K', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('d_k', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_ca', 'CA', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('d_ca', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('d_bicarbonato', 'Bicarbonato', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('d_bicarbonato', null, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>

					</div>
				</fieldset>
			</div>
			<div class="col-md-6">
				
				<fieldset>
					<legend class="text-semibold text-center">
						<i class="icon-copy position-left"></i>
						Parametros de laboratorio
						
					</legend>

					<div class="row form-horizontal">

						<div class="col-md-6">
							<div class="form-group form-group-xs">
								{!! Form::label('l_hto', 'HTO', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_hto', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_saturacion', '% de Saturación', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_saturacion', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_ferritina', 'Ferritina', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_ferritina', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_ktv', 'Kt/V', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_ktv', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_bun', 'Bun Pre-Post', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_bun', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_creatinina', 'Creatinina', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_creatinina', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_albumina', 'Albumina', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_albumina', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_ast', 'AST/ALT', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_ast', null, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-xs">
								{!! Form::label('l_k', 'K', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_k', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_ca', 'CA', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_ca', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_p', 'P', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_p', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_falcalina', 'F. Alcalina', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_falcalina', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_pth', 'PTH', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_pth', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_hiv', 'HIV', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_hiv', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_vhb', 'VHB', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_vhb', null, ['class' => 'form-control']) !!}
								</div>
							</div>

							<div class="form-group form-group-xs">
								{!! Form::label('l_vhc', 'VHC', ['class' => 'col-lg-6 control-label']) !!}
								<div class="col-lg-6">
									{!! Form::text('l_vhc', null, ['class' => 'form-control']) !!}
								</div>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

		<fieldset>
			<hr>

			<div class="row form-horizontal">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('tratamiento', '', ['class' => 'col-md-3 control-label']) !!}
						<div class="col-md-9">
							{!! Form::textArea('tratamiento', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('observaciones', '', ['class' => 'col-md-3 control-label']) !!}
						<div class="col-md-9">
							{!! Form::textArea('observaciones', null, ['class' => 'form-control', 'rows' => 3]) !!}
						</div>
					</div>

				</div>


			</div>
		</fieldset>
	</div>
	<div class="panel-footer text-right pr-20">
		<button type="submit" class="btn btn-primary">Actualizar informe <i class="icon-arrow-right14 position-right"></i></button>
	</div>
</div>
{!! Form::close() !!}

@include('ficham.elementos.modalPacienteCreate')
@endsection