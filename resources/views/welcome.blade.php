@extends('layouts.app')

@section('page-title', 'Panel principal')


@push('js-files')
{!! Html::script('assets/js/plugins/ui/moment/moment.min.js') !!}
{!! Html::script('assets/js/plugins/ui/fullcalendar/fullcalendar.min.js') !!}
{!! Html::script('assets/js/plugins/visualization/echarts/echarts.js') !!}
@endpush

@push('js')
{!! Html::script('assets/js/pages/extra_fullcalendar.js') !!}
{!! Html::script('js/dashboard.js') !!}
@endpush

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Estadisticas de atención diaria</h5>
			</div>

			<div class="panel-body">
				<div class="chart-container">
					<div class="chart has-fixed-height" id="basic_donut"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">

		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Calendario de Citas</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
						<li><a data-action="close"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<p class="content-group">Calendario de citas programadas para el médico registrado.</p>

				<div class="fullcalendar-basic"></div>
			</div>
		</div>
	</div>
</div>
<!-- /basic view -->
@endsection