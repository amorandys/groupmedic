
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GroupMedic</title>

	{{-- Global stylesheets --}}
    {!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') !!}
    {!! Html::style('assets/css/icons/icomoon/styles.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/css/core.css') !!}
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/colors.css') !!}
    {!! Html::style('assets/css/extras/animate.min.css') !!}
    {!! Html::style('css/app.css') !!}
    {!! Html::style('css/sweetalert.css') !!}
    {{-- /global stylesheets --}}

	{{-- Core JS files --}}
    {!! Html::script('assets/js/plugins/loaders/pace.min.js') !!}
    {!! Html::script('assets/js/core/libraries/jquery.min.js') !!}
    {!! Html::script('assets/js/core/libraries/bootstrap.min.js') !!}
    {!! Html::script('assets/js/plugins/loaders/blockui.min.js') !!}
    {{-- /core JS files --}}


    {!! Html::script('assets/js/core/app.js') !!}

</head>

<body class="login-container">

	<!-- Main navbar -->
    @include('layouts.elementos.navbar')
	
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<div class="text-center content-group">
						<h1 class="error-title">404</h1>
						<h5>Vaya, se ha producido un error. ¡Página no encontrada!</h5>
					</div>


					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">

								<div class="row">
									<div class="col-sm-6 col-md-offset-3">
										<a href="{{ route('index') }}" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Volver al inicio</a>
									</div>

									
								</div>
						</div>
					</div>


					<div class="footer text-muted text-center">
						&copy; 2017. <a href="#">SCCenMed</a> por <a href="http://scinformactica.cl" target="_blank">SC Informática</a>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>
</html>
