$(document).ready(function() {

	$.extend($.fn.pickatime.defaults, {
	  	clear: 'Limpiar',
		format: 'H:i',
  		min: [5,0],
  		max: [20,0],
  		disable: [
  			12
  		]
	})
	$('.pickatime').pickatime({
		
	});
});