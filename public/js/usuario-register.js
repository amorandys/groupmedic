

$(document).ready(function() {
    var url_regiones = $('a.region_id').attr('href'); 

    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    $('.pickadate').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        showMonthsShort: undefined,
        showWeekdaysFull: undefined,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        labelMonthNext: 'Mes siguiente',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecionar un mes',
        labelYearSelect: 'Selecionar un año',
        format: 'dd/mm/yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenPrefix: true,
        hiddenSuffix: '_submit',
        hiddenName: undefined,
        selectYears: true,
        selectMonths: true,
        firstDay: 1,
        selectYears: 70
    });

    var url_estados = $('a.pais_id').attr('href'); 
    var url_ciudades = $('a.ciudad_id').attr('href'); 

    $('select#tipo').on('change', function (evt) {
        var panel = $('#informacion_adicional')
        if ($(this).val() == 2) {
            panel.removeClass('hide')
            $('#group-nombres').removeClass('hide')
            $('#group-apellidos').removeClass('hide')
            $('#group-usuario').addClass('hide')
            $('#usuario').prop('disabled', 'disabled')
            $('#nombres').prop('disabled', '')
            $('#apellidos').prop('disabled', '')

            $('#rut').prop('disabled', '')
            $('#fecha_nacimiento').prop('disabled', '')
            $('#sexo').prop('disabled', '')
            $('#telefono').prop('disabled', '')
            $('#celular').prop('disabled', '')
            $('#region_id').prop('disabled', '')
            $('#comuna_id').prop('disabled', '')
            $('#direccion').prop('disabled', '')
        }else{
            $('#group-nombres').addClass('hide').prop('disabled', '')
            $('#group-apellidos').addClass('hide').prop('disabled', '')
            $('#group-usuario').removeClass('hide').prop('disabled', 'disabled')
            $('#usuario').prop('disabled', '')
            $('#nombres').prop('disabled', 'disabled')
            $('#apellidos').prop('disabled', 'disabled')

            $('#rut').prop('disabled', 'disabled')
            $('#fecha_nacimiento').prop('disabled', 'disabled')
            $('#sexo').prop('disabled', 'disabled')
            $('#telefono').prop('disabled', 'disabled')
            $('#celular').prop('disabled', 'disabled')
            $('#region_id').prop('disabled', 'disabled')
            $('#comuna_id').prop('disabled', 'disabled')
            $('#direccion').prop('disabled', 'disabled')
            panel.addClass('hide')
        }
    });

    $('#rut').formatter({
        pattern: '{{99}}.{{999}}.{{999}}',
        persistent:false
    });

    $('#telefono').formatter({
        pattern: '+56 2 {{9999}} {{9999}}',
        persistent:false
    });

    $('#celular').formatter({
        pattern: '+56 9 {{9999}} {{9999}}',
        persistent:false
    });

    $('#region_id').change(function(event) {
        $.ajax({
            url: url_regiones.replace(':ID', $(this).val()),
            dataType: 'json',
        })
        .done(function(data) {
            var html = ''
            $.each(data, function(index, val) {
                html += '<option value="'+index+'">'+val+'</option>'
            });
            $('#comuna_id').html(html)
        })
        .fail(function(data) {
            console.log(data);
        })
    });

        /*
    $('#frmUsuario').submit(function(event) {
        event.preventDefault()
        
        $(this).resetear();
        $('#informacion_adicional').addClass('hide')
        $(".styled").prop('checked', false)
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: 'json',
            data: $(this).serialize(),
        })
        .done(function(data) {
            window.location.href = $('#ruta_index').attr('href')
        })
        .fail(function(jqXhr, json, errorThrown) {
            var errors = jqXhr.responseJSON;
            var errorsHtml = '<ul>';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>'+value[0]+'</li>';
            });
            errorsHtml += '</ul>';

            new PNotify({
                title: 'Errores en la carga',
                text: errorsHtml,
                icon: 'icon-warning22'
            });
        })
    });

        */
});


function digitoVerificador(rut)
{

}

jQuery.fn.resetear = function () {
    $(this).each (function() { this.reset(); });
}
