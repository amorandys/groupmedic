$(function() {
	
});

function pacienteInfo(id)
{
	var url_paciente_show = $('.paciente_info').attr('href').replace(':ID', id);
	$.ajax({
		url: url_paciente_show,
		dataType: 'json',
	})
	.done(function(data) {
		$('span#paciente').html(data.paciente)
		$('span#rut').html(data.rut.replace(',', '.').replace(',', '.'))
		$('span#sexo').html(data.sexo)
		$('span#nacionalidad').html(data.nacionalidad)
		$('span#pais_nacimiento').html(data.pais_nacimiento)
		$('span#email').html('<a href="mailto:'+data.email+'">'+data.email+'</a>')
		$('span#telefono').html('<a href="tel:'+data.telefono+'">'+data.telefono+'</a>')
		$('span#celular').html('<a href="tel:'+data.celular+'">'+data.celular+'</a>')
		
		console.log(data);
	})
	.fail(function(data) {
		console.log(data);
	})
	.always(function() {
		$('#mdPacienteInfo').modal('show')
	});
	

}