require.config({
    paths: {
        echarts: 'assets/js/plugins/visualization/echarts'
    }
});

require(
    [
    'echarts',
    'echarts/theme/limitless',
    'echarts/chart/pie',
    'echarts/chart/funnel'
    ], 
    function (ec, limitless) {
        var basic_donut = ec.init(document.getElementById('basic_donut'), limitless);

        basic_donut_options = {

            title: {
                text: 'Pacientes Atendidos',
                subtext: 'estadisticas diarias',
                x: 'right'
            },

            legend: {
                orient: 'vertical',
                x: 'left',
                data: ['Atendidos', 'Por Atender']
            },

            calculable: true,

            series: [
            {
                name: 'Pacientes',
                type: 'pie',
                radius: ['50%', '70%'],
                center: ['50%', '57.5%'],
                itemStyle: {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true,
                            formatter: '{b}' + '\n\n' + '{c} ({d}%)',
                            position: 'center',
                            textStyle: {
                                fontSize: '17',
                                fontWeight: '500'
                            }
                        }
                    }
                },

                data: [
                {value: 17, name: 'Atendidos'},
                {value: 11, name: 'Por Atender'},
                ]
            }
            ]
        };

        basic_donut.setOption(basic_donut_options);

        window.onresize = function () {
            setTimeout(function (){
                basic_pie.resize();
                basic_donut.resize();
                nested_pie.resize();
                infographic_donut.resize();
                rose_diagram_hidden.resize();
                rose_diagram_visible.resize();
                lasagna_donut.resize();
                pie_timeline.resize();
                multiple_donuts.resize();
            }, 200);
        }
    });