$(function() {
    $('#rut, .rut').blur(function(event) {
        var str = $(this).val()

        if (str.length >= 8 && str.length <= 10 && str.match(/[-+]?([0-9]*\.[0-9]+|[0-9]+)/)) {
            
            var url = $('.digito-verificador').attr('href').replace(':RUT', str);
            
            $.ajax({
                url: url,
                dataType: 'json',
            })
            .done(function(data) {
                console.log(data)
                $('.input-group-addon').html('- '+data)
            })
            .fail(function(data) {
            })
        }else{
            $('.input-group-addon').html('- ')
        }
    });

    $('.rut').on('blur', function(event) {
        event.preventDefault()
        var rut = $(this).val()
        if (rut!= '') {
        $.ajax({
            url: $('a.comprobar').attr('href').replace(':ID', rut),
            dataType: 'json',
        })
        .done(function(data) {
            if (data.result == true) {
                $('#paciente_info').val(data.paciente.nombres+' '+data.paciente.apellidos)
            }else{
                $('#paciente_info').val('')
                var modal = $('#mdPaciente')
                modal.modal('show')
            }
        })
        .fail(function(data) {
            console.log(data);
        })
        
        }
    });

    $('.selectpicker').selectpicker()
});

