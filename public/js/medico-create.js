$(document).ready(function() {

    var url_estados = $('a.pais_id').attr('href'); 
    var url_ciudades = $('a.ciudad_id').attr('href'); 

    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice',
        //wrapperClass: 'border-primary-600 text-primary-800'
    });


    $('select').select2();

    $('#rut').formatter({
        pattern: '{{99}}.{{999}}.{{999}}-{{9}}'
    });

    $('#telefono').formatter({
        pattern: '+56 {{9}} {{9999}} {{9999}}'
    });

    $('#celular').formatter({
        pattern: '+56 {{9}} {{9999}} {{9999}}'
    });

    $('#pais_id').change(function(event) {
        $.ajax({
            url: url_estados.replace(':ID', $(this).val()),
            dataType: 'json',
        })
        .done(function(data) {
            var html = ''
            $.each(data, function(index, val) {
                html += '<option value="'+index+'">'+val+'</option>'
            });
            $('#region_id').html(html)
        })
        .fail(function(data) {
            console.log(data);
        })
    });


    $('#region_id').change(function(event) {
        $.ajax({
            url: url_ciudades.replace(':ID', $(this).val()),
            dataType: 'json',
        })
        .done(function(data) {
            var html = ''
            $.each(data, function(index, val) {
                html += '<option value="'+index+'">'+val+'</option>'
            });
            $('#ciudad_id').html(html)
        })
        .fail(function(data) {
            console.log(data);
        })
    });

    

});



function enviarFormulario()
{
    var form = $('form')
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        dataType: 'json',
        data: form.serialize(),
    })
    .done(function(data) {
        new PNotify({
            title: 'Registro almacenado',
            text: 'Los datos fueron almacenados satisfactoriamente',
            icon: 'icon-warning22'
        });
    })
    .fail(function(jqXhr, json, errorThrown) {
        var errors = jqXhr.responseJSON;
        var errorsHtml = '<ul>';
        $.each( errors, function( key, value ) {
            errorsHtml += '<li>'+value[0]+'</li>';
        });
        errorsHtml += '</ul>';

        new PNotify({
            title: 'Errores en la carga',
            text: errorsHtml,
            icon: 'icon-warning22'
        });
    })
}