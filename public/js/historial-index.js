$(document).ready(function() {

});

function mostrarEvolucion(id)
{
	$.ajax({
		url: $('.mostrarEvolucion').attr('href').replace(':ID', id),
		dataType: 'json',
	})
	.done(function(data) {
		console.log(data);
		$('#medico').html('<span class="text-semibold">' + data.medico.nombres +', '+data.medico.apellidos + '</span>')
		$('#sucursal').html(data.sucursal.nombre)
		$('#folio').html('FOLIO: ' + String('000000' + data.id).slice(-6))
		$('#fecha_informe').html('Fecha: <span class="text-semibold">' + data.fecha_informe +'</span>')
		$('#mdlEvolucion').modal('show')



		$('#estado').html(data.estado)
		$('#tolerancia_hd').html(data.tolerancia_hd)
		$('#compli_intra').html(data.compli_intra)
		$('#causa_compli_intra').html(data.causa_compli_intra)
		$('#peso_seco').html(data.peso_seco)


		$('#compli_cardio').html((data.compli_cardio == 1) ? 'Si' : 'No')
		$('#arritmia_causa').html(data.arritmia_causa)

		$('#arritmia_causa').html(data.arritmia_causa)
		$('#edo_hermatologico').html(data.edo_hermatologico)
		$('#edo_osteometabolico').html(data.edo_osteometabolico)
		$('#dep_urea').html(data.dep_urea)
		$('#acc_vascular').html(data.acc_vascular)
		$('#deriv_vit_d').html(data.deriv_vit_d)
		$('#edo_nutricional').html(data.edo_nutricional)

		$('#urgencia_hospitalizacion').html((data.urgencia_hospitalizacion == 1) ? 'Si' : 'No')
		$('#plan_terapeutico').html(data.plan_terapeutico)
		$('#transfusion_cantidad').html(data.transfusion_cantidad)
		$('#resuelto').html((data.resuelto == 1) ? 'Si' : 'No')

		$('a.inprmirInfEvolucion').attr('href', $('.infEvolucion').attr('href').replace(':ID', id));
	})
	.fail(function(data) {
		console.log(data);
	})
	
}