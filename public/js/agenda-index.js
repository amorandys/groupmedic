$(function() {
    var todayDate = moment().startOf('day');
    var YM = todayDate.format('YYYY-MM');
    var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    var TODAY = todayDate.format('YYYY-MM-DD');
    var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    $('.rut').formatter({
        pattern: '{{99}}.{{999}}.{{999}}',
    });

    var events = [
            {
                id:999,
                title: 'Patricio Rodríguez',
                start: YM + '-01'
            },
            {
                id:999,
                title: 'Fabiola Suazo',
                start: YM + '-10',
            },
            {
                id:999,
                title: 'Rosmar Blasquez',
                start: YM + '-09T16:00:00'
            },
            {
                id:999,
                title: 'Maria Lozada',
                start: YM + '-16T16:00:00'
            },
            {
                id:999,
                title: 'Rosmar Blasquez',
                start: YESTERDAY,
            },
            {
                id:999,
                title: 'Tania',
                start: TODAY + 'T10:30:00',
            },
            {
                id:999,
                title: 'Alessandra Arias',
                start: TODAY + 'T12:00:00'
            },
            {
                id:999,
                title: 'Raul Moya',
                start: TODAY + 'T14:30:00'
            },
            {
                id:999,
                title: 'Tania',
                start: TODAY + 'T20:00:00'
            },
            {
                id:999,
                title: 'Bryan Ojeda',
                start: TOMORROW + 'T07:00:00'
            },
            {
                id:999,
                title: 'Hernan Ortiz',
                start: YM + '-28'
            }
        ]




    $('.calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: new Date(),
        editable: true,
        events: events,
        lang: 'es',
        eventLimit: 2,
        navLinks: true,
        eventClick: function(calEvent, jsEvent, view) {

            console.log('Event: ' + calEvent.id);
            console.log('Event: ' + calEvent.title);
            console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            console.log('View: ' + view.name);


        }
    });

    
});